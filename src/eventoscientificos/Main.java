/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificosUI.JanelaPrincipal;
import java.io.IOException;
import utils.Utils;

/**
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            Empresa empresa = Utils.carregarEmpresaBinario();

            JanelaPrincipal janela = new JanelaPrincipal(empresa);
        } catch (IOException e) {
            Empresa empresa = new Empresa("Imp@acto");
            JanelaPrincipal janela = new JanelaPrincipal(empresa);
        } catch (ClassNotFoundException e) {
            Empresa empresa = new Empresa("Imp@acto");
            JanelaPrincipal janela = new JanelaPrincipal(empresa);
        }

    }

}
