/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificosUI.CriteriosMecanismo1;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa um mecanismo de distribuição de revisões de artigos
 * pelos revisores de um evento. O mecanismo segue os seguintes critérios
 * obrigatórios:
 * <p>
 * - Atribuição de três revisores a cada artigo;</p>
 * <p>
 * - Todos os revisores atribuídos a um artigo têm afinidade com um ou mais
 * tópicos do artigo.</p>
 *
 * @author Renato Ayres
 */
public class MecanismoDistribuicao1 implements MecanismoDistribuicao, Serializable {

    private ProcessoDistribuicao pd;

    /**
     * Cria uma instância de MecanismoDistribuicao, sem informação.
     */
    public MecanismoDistribuicao1() {
    }

    /**
     * Distribui as revisões sobre os artigos de acordo com as informações do
     * processo de distribuição passado por parâmetro e os critérios definidos
     * pelos métodos na classe.
     *
     * @param pd processo de distribuição que usa o mecanismo atual
     * @return lista de distribuições de revisões sobre os artigos
     */
    public List<Distribuicao> distribui(ProcessoDistribuicao pd) {
        this.pd = pd;
        List<Revisor> revisores = new ArrayList(pd.getEvento().getCp().getListaRevisores());
        List<Distribuicao> ldist = new ArrayList();
        for (Submissao s : pd.getEvento().getSubmissoes()) {
            List<Revisor> listaRevisoresArtigo = getRevisoresAfinidadeArtigo(pd.getTopicosAfinidade(), s.getArtigo(), revisores);
            List<Revisor> listaFinalRevisoresArtigo = setRevisoresArtigo(listaRevisoresArtigo, pd.getRevisoresArtigo());
            if (!(listaFinalRevisoresArtigo.isEmpty())) {
                ldist.add(new Distribuicao(s.getArtigo(), listaFinalRevisoresArtigo));
            }
        }

        return ldist;
    }

    /**
     * Devolve a lista de todos os revisores com pelo menos o número minimo,
     * passado por parâmetro, de tópicos em comum com o artigo passado por
     * parâmetro, e de acordo com a informação do evento passado por parâmetro.
     *
     * @param minimoTopicosAfinidade número mínimo de tópicos em comum entre os
     * revisores e o artigo
     * @param artigo artigo a ser analisado
     * @param revisores lista de revisores para trabalhar
     * @return lista de revisores com pelo menos o número mínimo, passado por
     * parâmetro, de tópicos em comum com o artigo passado por parâmetro
     */
    public List<Revisor> getRevisoresAfinidadeArtigo(int minimoTopicosAfinidade, Artigo artigo, List<Revisor> revisores) {
        List<Revisor> listaRevisoresArtigo = new ArrayList();
        Artigo artigoAtual = artigo;
        if (minimoTopicosAfinidade == 0) {
            return revisores;
        }
        for (Revisor revisor : revisores) {
            int afinidade = 0;
            for (Topico topicoArtigo : artigoAtual.getTopicos()) {
                for (Topico topicoRevisor : revisor.getTopicosPericia()) {
                    if (topicoRevisor.equals(topicoArtigo)) {
                        afinidade++;
                        break;
                    }
                }
                if (afinidade == minimoTopicosAfinidade) {
                    listaRevisoresArtigo.add(revisor);
                    break;
                }
            }
        }
        return listaRevisoresArtigo;
    }

    /**
     * Devolve uma lista de revisores, com o número de revisores a ser fixo (3),
     * e definido pelos critérios de distribuição da classe. Se o número de
     * revisores possíveis para rever o artigo for menor que três, todos são
     * adicionados.
     *
     * @param listaRevisoresArtigo lista de revisores possíveis de rever o
     * artigo
     * @param numRevisores número de revisores fixo para a revisão do artigo
     * @return lista de revisores do artigo
     */
    public List<Revisor> setRevisoresArtigo(List<Revisor> listaRevisoresArtigo, int numRevisores) {

        List<Revisor> listaFinalRevisoresArtigo = new ArrayList();
        if (listaRevisoresArtigo.size() < numRevisores) {
            numRevisores = listaRevisoresArtigo.size();
        }
        for (int i = 0; i < numRevisores; i++) {
            listaFinalRevisoresArtigo.add(listaRevisoresArtigo.get(i));
        }

        return listaFinalRevisoresArtigo;
    }

    /**
     * Devolve uma descrição textual sobre o mecanismo de distribuição,
     * inclusivé os critérios de distribuição.
     *
     * @return descrição do mecanismo de distribuição
     */
    @Override
    public String toString() {
        return "Mecanismo de Distribuição de Revisões num Evento Científico."
                + " Este mecanismo segue os seguintes critérios de cumprimento"
                + " obrigatório:\n\t- Atribuição de um número exato revisores a"
                + " cada artigo (predefinição é 3);"
                + "\n\t- Todos os revisores atribuídos a um artigo têm afinidade"
                + " com um número mínimo de tópicos do artigo (predefinição é 1).";
    }

    /**
     * Devolve o nome da classe
     *
     * @return nome da classe
     */
    public String getName() {
        return "MecanismoDistribuicao1";
    }

    /**
     * Abre uma janela de escolha de critérios de acordo com o processo de
     * distribuição e dos critérios da classe.
     *
     * @param pd processo de distribuição para trabalhar
     */
    public void openCriterios(ProcessoDistribuicao pd) {
        pd.setRevisoresArtigo(3);
        pd.setTopicosAfinidade(1);
        CriteriosMecanismo1 cm = new CriteriosMecanismo1(pd);
    }
}
