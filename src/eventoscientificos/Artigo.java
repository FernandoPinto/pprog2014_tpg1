/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Classe que representa um artigo com titulo, resumo, lista de autores, lista
 * de tópicos, autor correspondente e ficheiro.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Artigo implements Serializable {

    private String titulo;
    private String resumo;
    private String ficheiro;
    private Autor autorCorrespondente;
    private List<Autor> autores;
    private List<Topico> topicos;
    private static final String TITULO_POR_OMISSAO = "Sem título";
    private static final String RESUMO_POR_OMISSAO = "Sem resumo";
    private static final String FICHEIRO_POR_OMISSAO = "Sem ficheiro";

    /**
     * Constroi uma instancia de Artigo com titulo, resumo, autores e tópicos.
     *
     * @param autores autores do artigo
     * @param topicos tópicos do artigo
     * @param titulo título do artigo
     * @param resumo resumo do artigo
     */
    public Artigo(List<Autor> autores, List<Topico> topicos, String titulo, String resumo) {
        this.autores = new ArrayList(autores);
        this.topicos = new ArrayList(topicos);
        this.titulo = titulo;
        this.resumo = resumo;
    }

    /**
     * Constroi uma instância de Artigo recebendo titulo e resumo, e inicia as
     * listas de autores e topicos vazias.
     *
     * @param titulo titulo do artigo
     * @param resumo resumo do artigo
     */
    public Artigo(String titulo, String resumo) {
        this.titulo = titulo;
        this.resumo = resumo;
        autores = new ArrayList();
        topicos = new ArrayList();
    }

    /**
     * Constroi uma instância de artigo com o título, o resumo e nome do
     * ficheiro por omissão e inicializando as listas de autores e tópicos
     * vazias.
     */
    public Artigo() {
        titulo = Artigo.TITULO_POR_OMISSAO;
        resumo = Artigo.RESUMO_POR_OMISSAO;
        ficheiro = Artigo.FICHEIRO_POR_OMISSAO;
        autores = new ArrayList();
        topicos = new ArrayList();
    }

    /**
     * Constrói uma instância de Artigo com as mesmas características do artigo
     * passado por parâmetro.
     *
     * @param a artigo a copiar
     */
    public Artigo(Artigo a) {
        this.titulo = a.titulo;
        this.resumo = a.resumo;
        this.autores = new ArrayList(a.autores);
        this.autorCorrespondente = a.autorCorrespondente;
        this.topicos = new ArrayList(a.topicos);
        this.ficheiro = a.ficheiro;
    }

    /**
     * Devolve o título do artigo.
     *
     * @return título do artigo
     */
    public String getTitulo() {
        return this.titulo;
    }

    /**
     * Devolve o resumo do artigo.
     *
     * @return resumo do artigo
     */
    public String getResumo() {
        return this.resumo;
    }

    /**
     * Devolve o nome do ficheiro.
     *
     * @return nome do ficheiro
     */
    public String getFicheiro() {
        return this.ficheiro;
    }

    /**
     * Devolve o autor correspondente do artigo.
     *
     * @return autor correspondente do artigo
     */
    public Autor getAutorCorrespondente() {
        return this.autorCorrespondente;
    }

    /**
     * Devolve a lista de autores do artigo
     *
     * @return lista de autores do artigo
     */
    public List<Autor> getAutores() {
        return this.autores;
    }

    /**
     * Devolve a lista de tópicos do artigo
     *
     * @return lista de tópicos do artigo
     */
    public List<Topico> getTopicos() {
        return this.topicos;
    }

    /**
     * Modifica o título do artigo.
     *
     * @param titulo o novo título do artigo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Modifica o resumo do artigo.
     *
     * @param resumo o novo resumo do artigo
     */
    public void setResumo(String resumo) {
        this.resumo = resumo;
    }

    /**
     * Modifica o nome do ficheiro.
     *
     * @param ficheiro novo nome do ficheiro
     */
    public void setFicheiro(String ficheiro) {
        this.ficheiro = ficheiro;
    }

    /**
     * Define o autor corresponde para o artigo. Este autor tem que estar
     * associado a um utilizador do sistema.
     *
     * @param autorCorrespondente autor escolhido para corresponde do artigo
     */
    public void setAutorCorrespondente(Autor autorCorrespondente) {
        this.autorCorrespondente = autorCorrespondente;
    }

    /**
     * Modifica a lista de autores do artigo.
     *
     * @param autores nova lista de autores do artigo
     */
    public void setAutores(List<Autor> autores) {
        this.autores = new ArrayList(autores);
    }

    /**
     * Modifica a lista de tópicos associados ao artigo.
     *
     * @param topicos nova lista de tópicos associados ao artigo
     */
    public void setTopicos(List<Topico> topicos) {
        this.topicos = new ArrayList(topicos);
    }

    /**
     * Devolve uma descrição textual do artigo.
     *
     * @return descrição do artigo
     */
    @Override
    public String toString() {
        String aux = "Autores: ";
        for (ListIterator<Autor> it = autores.listIterator(); it.hasNext();) {
            aux += it.next().toString();
            if (it.hasNext()) {
                aux += ", ";
            }
        }
        aux += ".\nTópicos: ";
        for (ListIterator<Topico> it = topicos.listIterator(); it.hasNext();) {
            aux += it.next().toString();
            if (it.hasNext()) {
                aux += ", ";
            }
            aux += ".";
        }
        return String.format("\"%s\" \n %s", titulo, aux);
    }

    /**
     * Verifica se o artigo atual é igual ao objecto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Artigo a = (Artigo) obj;
        if (!(this.titulo.equalsIgnoreCase(a.titulo))) {
            return false;
        }
        boolean bool = false;
        for (Autor at : autores) {
            for (Autor atx : a.autores) {
                if (at.equals(atx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        for (Topico t : topicos) {
            for (Topico tx : a.topicos) {
                if (t.equals(tx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        return bool;
    }

    /**
     * Cria um novo autor, sem ser utitilizador, para o artigo atual.
     *
     * @param nome nome do autor
     * @param afiliacao instituição de afiliação do autor
     * @return novo autor do artigo
     */
    public Autor novoAutor(String nome, String afiliacao) {
        Autor autor = new Autor();

        autor.setNome(nome);
        autor.setAfiliacao(afiliacao);

        return autor;
    }

    /**
     * Cria um novo autor, associado a um utilizador, para o artigo final.
     *
     * @param nome nome do autor
     * @param afiliacao instituição de afiliação do autor
     * @param email email do autor
     * @param utilizador utilizador associado ao autor
     * @return novo autor do artigo
     */
    public Autor novoAutor(String nome, String afiliacao, String email, Utilizador utilizador) {
        Autor autor = new Autor();

        autor.setNome(nome);
        autor.setAfiliacao(afiliacao);
        autor.setEMail(email);
        autor.setUtilizador(utilizador);

        return autor;
    }

    /**
     * Adiciona um novo autor à lista de autores do artigo.
     *
     * @param autor autor a adicionar à lista de autores do artigo
     * @return o autor adicionado
     */
    public boolean addAutor(Autor autor) {
        return autores.add(autor);
    }

    /**
     * Método que retorna uma lista de autores com os autores que podem passar a
     * ser autor correspondente do artigo atual.
     *
     * @return lista com autores possíveis de serem autores correspondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.autores) {
            if (autor.podeSerCorrespondente()) {
                la.add(autor);
            }
        }
        return la;
    }

    /**
     * Associa um novo tópico ao artigo atual.
     *
     * @param t tópico a ser adicionado à lista de tópicos do artigo
     */
    public void addTopico(Topico t) {
        this.topicos.add(t);
    }
}
