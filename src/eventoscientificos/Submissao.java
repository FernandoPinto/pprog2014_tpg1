/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa uma submissão com artigo.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Submissao implements Serializable {

    private Artigo artigo;

    /**
     * Constrói uma instância de Submissão com artigo.
     *
     * @param a artigo da submissão
     */
    public Submissao(Artigo a) {
        this.artigo = new Artigo(a);
    }

    /**
     * Constrói uma instância de Submissão com as mesmas características da
     * submissão passada por parâmetro.
     *
     * @param s submissão a copiar
     */
    public Submissao(Submissao s) {
        this.artigo = new Artigo(s.artigo);
    }

    /**
     * Constrói uma instância de submissão sem informação.
     */
    public Submissao() {

    }

    /**
     * Devolve o artigo da submissão.
     *
     * @return artigo da submissão
     */
    public Artigo getArtigo() {
        return this.artigo;
    }

    /**
     * Modifica o artigo da submissão.
     *
     * @param artigo novo artigo da submissão
     */
    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }

    /**
     * Devolve uma informação textaul da submissão.
     *
     * @return informação da submissão
     */
    @Override
    public String toString() {
        return "Artigo: " + getArtigo().getTitulo();
    }

    /**
     * Verifica se a submissão atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Submissao s = (Submissao) obj;
        return this.artigo.equals(s.artigo);
    }

    /**
     * Cria e devolve um novo artigo.
     *
     * @return
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }
}
