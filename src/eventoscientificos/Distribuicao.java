/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Renato Ayres
 */
public class Distribuicao implements Serializable {

    private Artigo artigo;
    private List<Revisor> revisores;

    /**
     * Constrói uma instância de distribuição com artigo e lista de revisores.
     *
     * @param artigo artigo da distribuição
     * @param revisores lista de revisores da distribuição
     */
    public Distribuicao(Artigo artigo, List<Revisor> revisores) {
        this.artigo = artigo;
        this.revisores = new ArrayList(revisores);
    }

    /**
     * Constrói uma instância de Distribuicao sem informação, invoca o
     * construtor vazio na criação do artigo e inicializa a lista de revisores
     * vazia.
     */
    public Distribuicao() {
        this.artigo = new Artigo();
        this.revisores = new ArrayList();
    }

    /**
     * Constrói uma instância de Distribuicao com as mesmas características da
     * distribuição passada por parâmetro.
     *
     * @param d distribuição a copiar
     */
    public Distribuicao(Distribuicao d) {
        this.artigo = d.artigo;
        this.revisores = new ArrayList(d.revisores);
    }

    /**
     * Devolve o artigo da distribuição.
     *
     * @return artigo da distribuição
     */
    public Artigo getArtigo() {
        return artigo;
    }

    /**
     * Devolve a lista de revisores do artigo.
     *
     * @return lista de revisores do artigo
     */
    public List<Revisor> getRevisores() {
        return revisores;
    }

    /**
     * Modifica o artigo da distribuição.
     *
     * @param artigo novo artigo da distribuição
     */
    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }

    /**
     * Modifica a lista de revisores da distribuição.
     *
     * @param revisores nova lista de revisores da distribuição
     */
    public void setRevisores(List<Revisor> revisores) {
        this.revisores = new ArrayList(revisores);
    }

    /**
     * Devolve uma descrição textual da distribuição.
     *
     * @return descrição da distribuição
     */
    @Override
    public String toString() {
        String aux = "";
        for (Iterator<Revisor> it = revisores.iterator(); it.hasNext();) {
            Revisor r = it.next();
            aux += r.getNome();
            if (it.hasNext()) {
                aux += ", ";
            }
            aux += ".";
        }
        return String.format("Distribuição de revisões do artigo: %s\n\t Revisores: %s", artigo.getTitulo(), aux);
    }

    /**
     * Verifica se a distribuição atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Distribuicao d = (Distribuicao) obj;
        if (!(this.artigo.equals(d.artigo) && this.revisores.size() == d.revisores.size())) {
            return false;
        }
        boolean bool = false;
        for (Revisor r : revisores) {
            for (Revisor r1 : d.revisores) {
                if (r.equals(r1)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        return bool;
    }
}
