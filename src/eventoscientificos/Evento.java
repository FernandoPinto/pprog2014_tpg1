/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe que representa um evento com título, descrição, local, data de início,
 * data de fim, data limite de submissão, lista de organizadores, lista de
 * submissões, comissão de programa, lista de tópicos e processo de
 * distribuição.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Evento implements Serializable {

    private String titulo;
    private String descricao;
    private Local local;
    private String dataInicio;
    private String dataFim;
    private String dataLimiteSubmissão;
    private List<Organizador> organizadores;
    private List<Submissao> submissoes;
    private CP cp;
    private List<Topico> topicos;
    private ProcessoDistribuicao processoDistribuicao;

    /**
     * Constrói uma classe de Evento inicializando o local sem informação e as
     * listas de organizadores, submissões e tópicos vazias.
     */
    public Evento() {
        local = new Local();
        organizadores = new ArrayList<Organizador>();
        submissoes = new ArrayList<Submissao>();
        topicos = new ArrayList<Topico>();
        novaCP();
    }

    /**
     * Constrói uma instância de Evento com título e descrição e inicializando o
     * local sem informação e as listas de organizadores, submissões e tópicos
     * vazias.
     *
     * @param titulo título do evento
     * @param descricao descrição do evento
     */
    public Evento(String titulo, String descricao) {
        this.setTitulo(titulo);
        this.setDescricao(descricao);
        local = new Local();
        organizadores = new ArrayList<Organizador>();
        submissoes = new ArrayList<Submissao>();
        topicos = new ArrayList<Topico>();
        novaCP();
    }

    /**
     * Devolve o título do evento.
     *
     * @return título do evento
     */
    public String getTitulo() {
        return this.titulo;
    }

    /**
     * Devolve a descrição do evento.
     *
     * @return descrição do evento
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve o local do evento.
     *
     * @return local do evento
     */
    public Local getLocal() {
        return local;
    }

    /**
     * Devolve a data de início do evento.
     *
     * @return data de início do evento
     */
    public String getDataInicio() {
        return dataInicio;
    }

    /**
     * Devolve a data de fim do evento.
     *
     * @return data de fim do evento
     */
    public String getDataFim() {
        return dataFim;
    }

    /**
     * Devolve a data limite de submissão do evento.
     *
     * @return data limite de submissão do evento
     */
    public String getDataLimiteSubmissão() {
        return dataLimiteSubmissão;
    }

    /**
     * Devolve a lista de organizadores do evento.
     *
     * @return lista de organizadores do evento
     */
    public List<Organizador> getOrganizadores() {
        return organizadores;
    }

    /**
     * Devolve a lista de submissões do evento
     *
     * @return lista de submissões do evento
     */
    public List<Submissao> getSubmissoes() {
        return this.submissoes;
    }

    /**
     * Devolve a comissão de programa do evento.
     *
     * @return comissão de programa do evento
     */
    public CP getCp() {
        return this.cp;
    }

    /**
     * Devolve a lista de tópicos do evento.
     *
     * @return lista de tópicos do evento
     */
    public List<Topico> getTopicos() {
        return topicos;
    }

    /**
     * Devolve o processo de distribuição do evento.
     *
     * @return processo de distribuição do evento
     */
    public ProcessoDistribuicao getProcessoDistribuicao() {
        return this.processoDistribuicao;
    }

    /**
     * Modifica o título do evento.
     *
     * @param titulo novo título do evento
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * Modifica a descrição do evento.
     *
     * @param descricao nova descrição do evento
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Modifica o local do evento.
     *
     * @param local novo local do evento
     */
    public void setLocal(Local local) {
        this.local = local;
    }

    /**
     * Modifica a data de início do evento.
     *
     * @param dataInicio nova data de início do evento
     */
    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * Modifica a data de fim do evento.
     *
     * @param dataFim nova data de fim do evento
     */
    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * Modifica a data limite de submissão de artigos do evento.
     *
     * @param dataLimiteSubmissão nova data limite de submissão de artigos do
     * evento
     */
    public void setDataLimiteSubmissão(String dataLimiteSubmissão) {
        this.dataLimiteSubmissão = dataLimiteSubmissão;
    }

    /**
     * Modifica a lista de organizadores do evento.
     *
     * @param organizadores nova lista de organizadores do evento
     */
    public void setOrganizadores(List<Organizador> organizadores) {
        this.organizadores = organizadores;
    }

    /**
     * Modifica a lista de submissões do evento
     *
     * @param submissoes nova lista de submissões do evento
     */
    public void setSubmissoes(List<Submissao> submissoes) {
        this.submissoes = submissoes;
    }

    /**
     * Modifica a comissão de programa do evento.
     *
     * @param cp nova comissão de programa do evento
     */
    public void setCp(CP cp) {
        this.cp = cp;
    }

    /**
     * Modifica a lista de tópicos do evento.
     *
     * @param topicos nova lista de tópicos do evento
     */
    public void setTopicos(List<Topico> topicos) {
        this.topicos = topicos;
    }

    /**
     * Modifica o processo de distribuiçao do evento.
     *
     * @param processoDistribuicao novo processo de distribuição do evento
     */
    public void setProcessoDistribuicao(ProcessoDistribuicao processoDistribuicao) {
        this.processoDistribuicao = processoDistribuicao;
    }

    /**
     * Devolve uma descrição textual do evento.
     *
     * @return descrição do evento
     */
    @Override
    public String toString() {
        return String.format("%s, %s.", titulo, descricao);
    }

    /**
     * Verifica se o evento atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Evento e = (Evento) obj;
        if (!(this.titulo.equals(e.titulo) && this.descricao.equals(e.descricao)
                && this.local.equals(e.local) && this.dataInicio.equals(e.dataInicio)
                && this.dataFim.equals(e.dataFim) && this.dataLimiteSubmissão.equals(e.dataLimiteSubmissão)
                && this.organizadores.size() == e.organizadores.size()
                && this.submissoes.size() == e.submissoes.size()
                && this.topicos.size() == e.topicos.size() && this.cp.equals(e.cp)
                && this.processoDistribuicao.equals(e.processoDistribuicao))) {
            return false;
        }
        boolean bool = false;
        for (Organizador org : organizadores) {
            for (Organizador ogrx : e.organizadores) {
                if (org.equals(ogrx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        for (Submissao s : submissoes) {
            for (Submissao sx : e.submissoes) {
                if (s.equals(sx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        for (Topico t : topicos) {
            for (Topico tx : e.topicos) {
                if (t.equals(tx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        return bool;
    }

    /**
     * Adiciona um organizador ao evento.
     *
     * @param id Identificação do organizador
     * @param u utilizador associado ao organizador
     * @return true se possível adicionar o organizador, false se não
     */
    public boolean addOrganizador(String id, Utilizador u) {
        return addOrganizador(new Organizador(id, u));
    }

    /**
     * Adiciona um organizador à lista de organizadores do evento.
     *
     * @param o organizador a ser adicionado
     * @return true se possível adicionar, false se não
     */
    public boolean addOrganizador(Organizador o) {
        return organizadores.add(o);
    }

    /**
     * Cria e devolve uma nova submissão de artigo.
     *
     * @return nova submissão
     */
    public Submissao novaSubmissao() {
        return new Submissao();
    }

    /**
     * Adiciona uma submissão à lista de submissões do evento.
     *
     * @param submissao submissão a adicionar
     * @return true se possível adicionar, false se não
     */
    public boolean addSubmissao(Submissao submissao) {
        return this.submissoes.add(submissao);
    }

    private CP novaCP() {
        cp = new CP();

        return cp;
    }

    /**
     * Adiciona um revisor à comissão de programa do evento.
     *
     * @param r revisor a adicionar
     * @return true se bem sucedido a adicionar, false se não
     */
    public boolean addRevisor(Revisor r) {
        return cp.registaMembroCP(r);
    }

    /**
     * Cria e devolve um novo tópico.
     *
     * @return novo tópico
     */
    public Topico novoTopico() {
        return new Topico();
    }

    /**
     * Adiciona um tópico à lista de tópicos do evento.
     *
     * @param t tópico a adicionar
     * @return true se possível adicionar, false se não
     */
    public boolean addTopico(Topico t) {
        return this.topicos.add(t);
    }

    /**
     * Cria e devolve um novo processo de distribuição com o evento atual e um
     * mecanismo associados.
     *
     * @param m mecanismo do processo de distribuição
     * @return novo processo de distribuição
     */
    public ProcessoDistribuicao novoProcessoDistribuicao(MecanismoDistribuicao m) {
        return new ProcessoDistribuicao(this, m);
    }

}
