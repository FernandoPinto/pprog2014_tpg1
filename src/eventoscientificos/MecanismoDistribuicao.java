/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 * Interface de apoio à distribuição de revisões de artigos num evento
 * científico.
 *
 * @author Renato Ayres
 */
public interface MecanismoDistribuicao {

    /**
     * Realiza a distribuição de acordo com o processo de distribuição e dos
     * critérios da classe.
     *
     * @param pd processo de distribuição para trabalhar
     * @return lista de distribuições de revisões de artigos
     */
    public List<Distribuicao> distribui(ProcessoDistribuicao pd);

    /**
     * Devolve o nome da classe em que se está a trabalhar.
     *
     * @return nome da classe
     */
    public String getName();

    /**
     * Abre uma janela de escolha de critérios de acordo com o processo de
     * distribuição e dos critérios da classe.
     *
     * @param pd processo de distribuição para trabalhar
     */
    public void openCriterios(ProcessoDistribuicao pd);
}
