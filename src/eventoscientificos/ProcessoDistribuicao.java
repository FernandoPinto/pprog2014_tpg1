/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.List;

/**
 * Classe que representa um processo de distribuição com evento e mecanismo de
 * distribuição associados, número de tópicos de afinidade entre revisor e
 * artigo, número de revisores por artigo e número de revisores com afinidade
 * com o artigo. Ajuda na distribuição de revisões de artigos em eventos de
 * acordo com os critérios definidos na classe e no mecanismo de distribuição.
 *
 * @author Reventonato Ayreventos
 */
public class ProcessoDistribuicao {

    private Evento evento;
    private MecanismoDistribuicao mecanismo;
    private int topicosAfinidade;
    private int revisoresArtigo;
    private int revisoresAfinidade;

    /**
     * Constrói uma instância de processo de distribuição com evento e mecanismo
     * de distribuição associados.
     *
     * @param e evento a associar
     * @param m mecanismo de distribuição a associar
     */
    public ProcessoDistribuicao(Evento e, MecanismoDistribuicao m) {
        this.evento = e;
        this.mecanismo = m;
    }

    /**
     * Devolve o evento associado ao processo de distribuição.
     *
     * @return evento associado
     */
    public Evento getEvento() {
        return evento;
    }

    /**
     * Devolve o mecanismo de distribuição associado ao processo de
     * distribuição.
     *
     * @return mecanismo de distribuição associado
     */
    public MecanismoDistribuicao getMecanismo() {
        return mecanismo;
    }

    /**
     * Número de tópicos que o revisor, ou os revisores, devem ter em comum com
     * o artigo.
     *
     * @return número de tópicos em comum
     */
    public int getTopicosAfinidade() {
        return topicosAfinidade;
    }

    /**
     * Número de revisores por artigo.
     *
     * @return número de revisores por artigo
     */
    public int getRevisoresArtigo() {
        return revisoresArtigo;
    }

    /**
     * Número de revisores com afinidade com o artigo, de acordo com o número de
     * tópicos estabelecido.
     *
     * @return número de revisores com afinidade com o artigo
     */
    public int getRevisoresAfinidade() {
        return revisoresAfinidade;
    }

    /**
     * Modifica o evento do processo de distribuição
     *
     * @param evento novo evento do processo de distribuição
     */
    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    /**
     * Modifica o mecanismo de distribuição associado ao processo de
     * distribuição.
     *
     * @param mecanismo novo mecanismo de distribuição
     */
    public void setMecanismo(MecanismoDistribuicao mecanismo) {
        this.mecanismo = mecanismo;
    }

    /**
     * Modifica o número de tópicos que o revisor, ou os revisores, devem ter em
     * comum com o artigo.
     *
     * @param topicosAfinidade novo número de tópicos em comum
     */
    public void setTopicosAfinidade(int topicosAfinidade) {
        this.topicosAfinidade = topicosAfinidade;
    }

    /**
     * Modifica o número de revisores por artigo.
     *
     * @param revisoresArtigo novo número de revisores por artigo.
     */
    public void setRevisoresArtigo(int revisoresArtigo) {
        this.revisoresArtigo = revisoresArtigo;
    }

    /**
     * Modifica o número de revisores com afinidade com o artigo, de acordo com
     * o número de tópicos estabelecido.
     *
     * @param revisoresAfinidade novo número de revisores com afinidade
     */
    public void setRevisoresAfinidade(int revisoresAfinidade) {
        this.revisoresAfinidade = revisoresAfinidade;
    }

    /**
     * Devolve uma descrição textual do processo de distribuição.
     *
     * @return descrição do processo de dstribuição
     */
    @Override
    public String toString() {
        return String.format("Processe de distribuição do evento %s, com o %s.", evento.getTitulo(), mecanismo.getName());
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    /**
     * Verifica se o processo de distribuição atual é igual ao objeto passado
     * por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass().equals(obj.getClass())) {
            return false;
        }
        ProcessoDistribuicao pd = (ProcessoDistribuicao) obj;
        return this.evento.equals(pd.evento) && this.mecanismo.getName().equals(pd.getMecanismo().getName());
    }

    /**
     * Devolve a lista de distribuições sem as registar no evento.
     *
     * @return lista de distribuições
     */
    public List<Distribuicao> distribui() {
        List<Distribuicao> distribuicoes;
        distribuicoes = mecanismo.distribui(this);
        return distribuicoes;
    }
}
