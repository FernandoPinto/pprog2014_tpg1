package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Classe que representa uma empresa com uma lista de utilizadores e uma lista
 * de eventos científicos.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Empresa implements Serializable {

    private String nome;
    private List<Utilizador> utilizadores;
    private List<Evento> eventos;
    private List<MecanismoDistribuicao> mecanismos;
    private static final String NOME_POR_OMISSAO = "Sem nome";

    /**
     * Constrói uma instância de Empresa com nome, lista de utilizadores e lista
     * de eventos.
     *
     * @param nome nome da empresa
     * @param utilizadores lista de utilizadores da empresa
     * @param eventos lista de eventos da empresa
     */
    public Empresa(String nome, List<Utilizador> utilizadores, List<Evento> eventos) {
        this.nome = nome;
        this.utilizadores = new ArrayList(utilizadores);
        this.eventos = new ArrayList(eventos);
        mecanismos = new ArrayList();
        mecanismos.add(new MecanismoDistribuicao1());
        mecanismos.add(new MecanismoDistribuicao2());
    }

    /**
     * Constrói uma instãncia de Empresa inicializando as listas de utilizadores
     * e eventos vazias.
     *
     * @param nome nome da empresa
     */
    public Empresa(String nome) {
        this.nome = nome;
        utilizadores = new ArrayList();
        eventos = new ArrayList();
        mecanismos = new ArrayList();
        mecanismos.add(new MecanismoDistribuicao1());
        mecanismos.add(new MecanismoDistribuicao2());
    }

    /**
     * Constrói uma instância de Empresa com o nome por omissão e inicializa as
     * listas de utilizadores e de eventos vazias.
     */
    public Empresa() {
        this.nome = Empresa.NOME_POR_OMISSAO;
        utilizadores = new ArrayList();
        eventos = new ArrayList();
        mecanismos = new ArrayList();
        mecanismos.add(new MecanismoDistribuicao1());
        mecanismos.add(new MecanismoDistribuicao2());
    }

    /**
     * Devolve o nome da empresa.
     *
     * @return nome da empresa
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Devolve a lista de utilizadores da empresa.
     *
     * @return lista de utilizadores da empresa
     */
    public List<Utilizador> getUtilizadores() {
        return this.utilizadores;
    }

    /**
     * Devolve a lista de eventos da empresa.
     *
     * @return lista de eventos da empresa
     */
    public List<Evento> getEventos() {
        return this.eventos;
    }

    /**
     * Devolve a lista de mecanismos disponíveis na empresa.
     *
     * @return lista de mecanismos
     */
    public List<MecanismoDistribuicao> getMecanismos() {
        mecanismos = new ArrayList();
        mecanismos.add(new MecanismoDistribuicao1());
        mecanismos.add(new MecanismoDistribuicao2());
        return mecanismos;
    }

    /**
     * Modifica o nome da empresa.
     *
     * @param nome novo nome da empresa
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Modifica a lista de utilizadores da empresa.
     *
     * @param utilizadores nova lista de utilizadores da empresa
     */
    public void setUtilizadores(List<Utilizador> utilizadores) {
        this.utilizadores = new ArrayList(utilizadores);
    }

    /**
     * Modifica a lista de eventos da empresa.
     *
     * @param eventos nova lista de eventos da empresa
     */
    public void setEventos(List<Evento> eventos) {
        this.eventos = new ArrayList(eventos);
    }

    /**
     * Modifica os mecanismos disponíveis da empresa.
     *
     * @param mecanismos nova lista de mecanismos da empresa
     */
    public void setMecanismos(List<MecanismoDistribuicao> mecanismos) {
        this.mecanismos = mecanismos;
    }

    /**
     * Devolve uma descrição textual da empresa.
     *
     * @return descrição da empresa
     */
    @Override
    public String toString() {
        return "Empresa: " + this.nome;
    }

    /**
     * Verifica se a empresa atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Empresa e = (Empresa) obj;
        if (!(this.nome.equalsIgnoreCase(e.nome) && this.utilizadores.size() == e.utilizadores.size() && this.eventos.size() == e.eventos.size())) {
            return false;
        }
        boolean bool = false;
        for (Utilizador ut : utilizadores) {
            for (Utilizador utx : e.utilizadores) {
                if (ut.equals(utx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        for (Evento et : eventos) {
            for (Evento etx : e.eventos) {
                if (et.equals(etx)) {
                    bool = true;
                    break;
                }
                bool = false;
            }
        }
        return bool;
    }

    /**
     * Adiciona um utilizador à lista de utilizadores da empresa.
     *
     * @param u utilizador a adicionar
     * @return true se adicionado com sucesso, false se não
     */
    public boolean addUtilizador(Utilizador u) {
        return utilizadores.add(u);
    }

    /**
     * Adiciona todos os utilizadores passados por parâmetro como lista, à lista
     * de utilizadores da empresa.
     *
     * @param utilizadores utilizadores a adicionar à lista de utilizadores da
     * empresa
     */
    public void addUtilizadores(List<Utilizador> utilizadores) {
        this.utilizadores.addAll(utilizadores);
    }

    /**
     * Devolve o utilizador pretendido, se existir, através da identificação do
     * nome.
     *
     * @param id identificação do utilizador
     * @return utilizador pretendido, se não existir devolve null
     */
    public Utilizador getUtilizador(String id) {
        for (Utilizador u : this.utilizadores) {
            String aux = u.getUsername();
            if (aux.equals(id)) {
                return u;
            }
        }
        return null;
    }

    /**
     * Devolve o utilizador pretendido, se existir, através da identificação do
     * email.
     *
     * @param email identificação do utilizador
     * @return utilizador pretendido, se não existir devolve null
     */
    public Utilizador getUtilizadorEmail(String email) {
        for (Utilizador u : this.utilizadores) {
            String aux = u.getEmail();
            if (aux.equals(email)) {
                return u;
            }
        }

        return null;
    }

    /**
     * Adiciona um evento à lista de eventos da empresa.
     *
     * @param e evento a adicionar
     * @return true se adicionado com sucesso, false se não
     */
    public boolean addEvento(Evento e) {
        return eventos.add(e);
    }

    /**
     * Devolve o evento pretendido através da identificação do título.
     *
     * @param aux identificação do evento
     * @return o evento pretendido, se existir, null se não
     */
    public Evento getEvento(String aux) {
        for (Evento e : eventos) {
            if (aux.equals(e.getTitulo())) {
                return e;
            }
        }
        return null;
    }

    /**
     * Adiciona todos os eventos passados por parâmetro como lista, à lista de
     * eventos da empresa.
     *
     * @param eventos eventos a adicionar à lista de utilizadores da empresa
     */
    public void addEventos(List<Evento> eventos) {
        this.eventos.addAll(eventos);
    }

    /**
     * Devolve os eventos de um organizador, através da identificação do nome.
     *
     * @param id identificação do organizador
     * @return lista de eventos do organizador
     */
    public List<Evento> getEventosOrganizador(String id) {
        List<Evento> leOrganizador = new ArrayList<Evento>();

        Utilizador u = getUtilizador(id);

        if (u != null) {
            for (Iterator<Evento> it = eventos.listIterator(); it.hasNext();) {
                Evento e = it.next();
                List<Organizador> lOrg = e.getOrganizadores();

                boolean bRet = false;
                for (Organizador org : lOrg) {
                    if (org.getUtilizador().equals(u)) {
                        bRet = true;
                        break;
                    }
                }
                if (bRet) {
                    leOrganizador.add(e);
                }
            }
        }
        return leOrganizador;
    }

    /**
     * Devolve todos os eventos que já sofreram distribuição de revisões de
     * artigos.
     *
     * @param eventos eventos a verificar
     * @return lista de eventos já distribuídos
     */
    public List<Evento> getEventosDistribuidos(List<Evento> eventos) {
        List<Evento> list = new ArrayList();
        for (Evento e : eventos) {
            if (e.getProcessoDistribuicao() != null) {
                list.add(e);
            }
        }
        return list;
    }

    /**
     * Devolve todos os organizadores envolvidos nos eventos da empresa.
     *
     * @return todos os organizadores existentes
     */
    public List<Organizador> getOrganizadores() {
        List<Organizador> list = new ArrayList();
        for (Evento e : eventos) {
            for (Organizador organizador : e.getOrganizadores()) {
                if (!(list.contains(organizador))) {
                    list.add(organizador);
                }
            }
        }
        return list;
    }

    /**
     * Devolve todos os revisores envolvidos nos eventos da empresa.
     *
     * @return todos os revisores existentes
     */
    public List<Revisor> getRevisores() {
        List<Revisor> list = new ArrayList();
        for (Evento e : eventos) {
            for (Revisor revisor : e.getCp().getListaRevisores()) {
                if (!(list.contains(revisor))) {
                    list.add(revisor);
                }
            }
        }
        return list;
    }

    /**
     * Devolve todas as submissões dos eventos da empresa.
     *
     * @return todas as submissões existentes
     */
    public List<Submissao> getSubmissoes() {
        List<Submissao> list = new ArrayList();
        for (Evento e : eventos) {
            for (Submissao s : e.getSubmissoes()) {
                if (!(list.contains(s))) {
                    list.add(s);
                }
            }
        }
        return list;
    }

    /**
     * Devolve todos os autores envolvidos nas submissões dos eventos da
     * empresa.
     *
     * @return todos os autores existentes
     */
    public List<Autor> getAutores() {
        List<Autor> list = new ArrayList();
        for (Evento e : eventos) {
            for (Submissao s : e.getSubmissoes()) {
                for (Autor a : s.getArtigo().getAutores()) {
                    if (!(list.contains(a))) {
                        list.add(a);
                    }
                }
            }
        }
        return list;
    }

    /**
     * Devolve todos os tópicos de evento, dos eventos da empresa.
     *
     * @return todos os tópicos existentes
     */
    public List<Topico> getTopicos() {
        List<Topico> list = new ArrayList();
        for (Evento e : eventos) {
            for (Topico t : e.getTopicos()) {
                if (!(list.contains(t))) {
                    list.add(t);
                }
            }
        }
        return list;
    }
}
