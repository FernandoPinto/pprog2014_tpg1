/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import eventoscientificosUI.CriteriosMecanismo2;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Renato Ayres
 */
public class MecanismoDistribuicao2 implements MecanismoDistribuicao, Serializable {

    private ProcessoDistribuicao pd;

    /**
     * Cria uma instância de MecanismoDistribuicao, sem informação.
     */
    public MecanismoDistribuicao2() {
    }

    /**
     * Distribui as revisões sobre os artigos de acordo com as informações do
     * processo de distribuição passado por parâmetro e os critérios definidos
     * pelos métodos na classe.
     *
     * @param pd processo de distribuição que usa o mecanismo atual
     * @return lista de distribuições de revisões sobre os artigos
     */
    public List<Distribuicao> distribui(ProcessoDistribuicao pd) {
        this.pd = pd;
        List<Distribuicao> ldist = new ArrayList();
        List<Revisor> listaRevisoresAfinidade = new ArrayList();
        List<Revisor> listaRevisoresRestante = new ArrayList();

        List<Revisor> revisores = new ArrayList(pd.getEvento().getCp().getListaRevisores());

        for (Submissao s : pd.getEvento().getSubmissoes()) {
            List<Revisor> todosRevisoresAfinidade = getRevisoresAfinidadeArtigo(pd.getTopicosAfinidade(), s.getArtigo(), revisores);

            listaRevisoresAfinidade = getRevisoresAfinidadeArtigo(pd.getTopicosAfinidade(), s.getArtigo(), listaRevisoresAfinidade);
            if (listaRevisoresAfinidade.size() < pd.getRevisoresAfinidade()) {
                listaRevisoresAfinidade.addAll(getRevisoresAfinidadeArtigo(pd.getTopicosAfinidade(), s.getArtigo(), revisores));
            }

            listaRevisoresRestante = getRevisoresRestantes(s.getArtigo(), listaRevisoresRestante, pd.getTopicosAfinidade());
            if (listaRevisoresRestante.size() < (pd.getRevisoresArtigo() - pd.getRevisoresAfinidade())) {
                listaRevisoresRestante.addAll(getRevisoresRestantes(s.getArtigo(), revisores, pd.getTopicosAfinidade()));
            }

            List<Revisor> listaFinalRevisoresArtigo = setRevisoresArtigo(pd.getRevisoresArtigo(), pd.getRevisoresAfinidade(), listaRevisoresAfinidade, listaRevisoresRestante, todosRevisoresAfinidade);
            if (!(listaFinalRevisoresArtigo.isEmpty())) {
                ldist.add(new Distribuicao(s.getArtigo(), listaFinalRevisoresArtigo));
            }
        }
        return ldist;
    }

    /**
     * Devolve a lista de todos os revisores com pelo menos o número minimo,
     * passado por parâmetro, de tópicos em comum com o artigo passado por
     * parâmetro, e de acordo com a informação do processo de distribuição
     * responsável por ordenar a distribuição atual.
     *
     * @param minimoTopicosAfinidade número mínimo de tópicos em comum entre os
     * revisores e o artigo
     * @param artigo artigo a ser analisado
     * @param revisores lista de revisores para trabalhar
     * @return lista de revisores com pelo menos o número mínimo, passado por
     * parâmetro, de tópicos em comum com o artigo passado por parâmetro
     */
    public List<Revisor> getRevisoresAfinidadeArtigo(int minimoTopicosAfinidade, Artigo artigo, List<Revisor> revisores) {
        List<Revisor> listaRevisoresArtigo = new ArrayList();
        Artigo artigoAtual = artigo;
        if (minimoTopicosAfinidade == 0) {
            return revisores;
        }
        for (Revisor revisor : revisores) {
            int afinidade = 0;
            for (Topico topicoArtigo : artigoAtual.getTopicos()) {
                for (Topico topicoRevisor : revisor.getTopicosPericia()) {
                    if (topicoRevisor.equals(topicoArtigo)) {
                        afinidade++;
                        break;
                    }
                }
                if (afinidade == minimoTopicosAfinidade) {
                    listaRevisoresArtigo.add(revisor);
                    break;
                }
            }
        }
        return listaRevisoresArtigo;
    }

    /**
     * Devolve a lista de todos os revisores que nâo têm afinidade com o artigo.
     *
     * @param artigo artigo a analisar
     * @param revisores lista de revisores para trabalhar
     * @param minimoTopicosAfinidade número mínimo de tópicos em comum com o
     * artigo
     * @return lista de revisores que não cumprem o requisito mínimo de
     * afinidade com o artigo
     */
    public List<Revisor> getRevisoresRestantes(Artigo artigo, List<Revisor> revisores, int minimoTopicosAfinidade) {
        List<Revisor> listaRevisoresArtigoRestante = new ArrayList();
        Artigo artigoAtual = artigo;
        for (Revisor revisor : revisores) {
            int afinidade = 0;
            for (Topico topicoArtigo : artigoAtual.getTopicos()) {
                for (Topico topicoRevisor : revisor.getTopicosPericia()) {
                    if (topicoRevisor.equals(topicoArtigo)) {
                        afinidade++;
                        break;
                    }
                    if (afinidade > minimoTopicosAfinidade) {
                        break;
                    }
                }
            }
            if (afinidade == 0) {
                listaRevisoresArtigoRestante.add(revisor);
            }
        }
        return listaRevisoresArtigoRestante;
    }

    /**
     * Devolve uma lista de revisores, com o número de revisores a ser definido
     * pelos critérios de distribuição da classe.
     *
     * @param minimoRevisoresArtigo número mínimo de revisores por artigo
     * @param minimoRevisoresAfinidade número mínimo de revisores que cumprem o
     * requisito mínimo de afinidade com o artigo
     * @param listaRevisoresAfinidade lista de todos os revisores que cumprem o
     * requisito mínimo de afinidade com o artigo
     * @param listaRevisoresRestante lista de todos os revisores que não cumprem
     * o requisito mínimo de afinidade com o artigo
     * @return
     */
    public List<Revisor> setRevisoresArtigo(int minimoRevisoresArtigo, int minimoRevisoresAfinidade, List<Revisor> listaRevisoresAfinidade, List<Revisor> listaRevisoresRestante, List<Revisor> listaTodosRevisoresAfinidade) {
        List<Revisor> listaFinalRevisoresArtigo = new ArrayList();
        if (listaRevisoresRestante.isEmpty()) {
            minimoRevisoresAfinidade = minimoRevisoresArtigo;
        }
        for (int i = 0; i < minimoRevisoresAfinidade; i++) {
            if (listaRevisoresAfinidade.isEmpty()) {
                for (int j = 0; j < minimoRevisoresAfinidade; j++) {
                    listaFinalRevisoresArtigo.add(listaTodosRevisoresAfinidade.remove(0));
                }
                break;
            }
            listaFinalRevisoresArtigo.add(listaRevisoresAfinidade.remove(0));
        }
        for (int i = 0; i < minimoRevisoresArtigo - minimoRevisoresAfinidade; i++) {
            if (listaRevisoresRestante.isEmpty()) {
                break;
            }
            listaFinalRevisoresArtigo.add(listaRevisoresRestante.remove(0));
        }
        return listaFinalRevisoresArtigo;
    }

    /**
     * Devolve uma descrição textual sobre o mecanismo de distribuição,
     * inclusivé os critérios de distribuição.
     *
     * @return descrição do mecanismo de distribuição
     */
    @Override
    public String toString() {
        return "Mecanismo de Distribuição de Revisões num Evento Científico."
                + " Este mecanismo segue os seguintes critérios de cumprimento"
                + " obrigatório:\n\t- Atribuição de um número mínimo de "
                + "revisores a cada artigo(predefinição é 1);"
                + "\n\t- Um número mínimo de revisores (predefinição é 1)"
                + " atribuídos a um artigo"
                + " tem afinidade com um número mínimo (predefinição é 1) de"
                + " tópicos do artigo.\n\t\t"
                + "Segue também o seguinte critério opcional:"
                + "\n\t- Distribuição equitativa de artigos pelos revisores.";
    }

    /**
     * Devolve o nome da classe
     *
     * @return nome da classe
     */
    public String getName() {
        return "MecanismoDistribuicao2";
    }

    /**
     * Abre uma janela de escolha de critérios de acordo com o processo de
     * distribuição e dos critérios da classe.
     *
     * @param pd processo de distribuição para trabalhar
     */
    public void openCriterios(ProcessoDistribuicao pd) {
        pd.setRevisoresAfinidade(1);
        pd.setRevisoresArtigo(2);
        pd.setTopicosAfinidade(1);
        CriteriosMecanismo2 cm = new CriteriosMecanismo2(pd);
    }

}
