package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa um utilizador com nome, username, password e email.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public final class Utilizador implements Serializable {

    private String nome;
    private String username;
    private String password;
    private String email;

    /**
     * Constrói uma instância de utilizador com username, password, nome e
     * email.
     *
     * @param username username do utilizador
     * @param pwd password do utilizador
     * @param nome nome do utilizador
     * @param email email do utilizador
     */
    public Utilizador(String username, String pwd, String nome, String email) {
        this.setUsername(username);
        this.setPassword(pwd);
        this.setNome(nome);
        this.setEmail(email);
    }

    /**
     * Constrói uma instância de utilizador sem informação
     */
    public Utilizador() {
    }

    /**
     * Constrói uma instância de utilizador com as mesmas informações que o
     * utilizador passado por parâmetro.
     *
     * @param u utilizador a copiar
     */
    public Utilizador(Utilizador u) {
        this.nome = u.nome;
        this.email = u.email;
        this.username = u.username;
        this.password = u.password;
    }

    /**
     * Devolve o nome do utilizador
     *
     * @return nome do utilizador
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve o username do utilizador.
     *
     * @return username do utilizador
     */
    public String getUsername() {
        return username;
    }

    /**
     * Devolve a password do utilizador.
     *
     * @return password do utilizador
     */
    public String getPassword() {
        return password;
    }

    /**
     * Devolve o email do utilizador.
     *
     * @return email do utilizador
     */
    public String getEmail() {
        return email;
    }

    /**
     * Modifica o nome do utilizador.
     *
     * @param nome novo nome do utilizador
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Modifica o username do utilizador.
     *
     * @param username novo username do utilizador
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Modifica a password do utilizador.
     *
     * @param password nova password do utilizador
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Modifica o email do utilizador.
     *
     * @param email novo email do utilizador
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Devolve uma descrição textual do utilizador.
     *
     * @return descrição do utilizador
     */
    @Override
    public String toString() {
        String aux = "Utilizador:\n";
        aux += "\tNome: " + this.nome + "\n";
        aux += "\tUsername: " + this.username + "\n";
        aux += "\tPassword: " + this.password + "\n";
        aux += "\tEmail: " + this.email + "\n";

        return aux;
    }

    /**
     * Verifica se o utilizador atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Utilizador u = (Utilizador) obj;
        return this.username.equals(u.username) || this.email.equals(u.email);
    }
}
