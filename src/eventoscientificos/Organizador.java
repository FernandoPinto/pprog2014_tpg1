/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa um organizador com nome e utilizador associado.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Organizador implements Serializable {

    private String nome;
    private Utilizador utilizador;

    /**
     * Constrói uma instância de organizador com nome e utilizador associado.
     *
     * @param id identificação do organizador
     * @param u utilizador a associar
     */
    public Organizador(String id, Utilizador u) {
        nome = u.getNome();
        utilizador = u;
    }

    /**
     * Constrói uma instância de Organizador com utilizador associado.
     *
     * @param u utilizador a associar
     */
    public Organizador(Utilizador u) {
        nome = u.getNome();
        this.utilizador = u;
    }

    /**
     * Devolve o nome do organizador.
     *
     * @return nome do organizador
     */
    public String getNome() {
        return nome;
    }

    /**
     * Devolve o utilizador associado do organizador.
     *
     * @return utilizador associado do organizador
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Modifica o nome do organizador
     *
     * @param nome novo nome do organizador
     */
    public void setNome(String nome) {
        this.nome = nome;
        utilizador.setNome(nome);
    }

    /**
     * Modifica o utilizador associado do organizador.
     *
     * @param u novo utilizador associado
     */
    public void setUtilizador(Utilizador u) {
        utilizador = u;
        nome = u.getNome();
    }

    /**
     * Devolve uma descrição textual do organizador.
     *
     * @return descrição do organizador
     */
    @Override
    public String toString() {
        return utilizador.toString();
    }

    /**
     * Verifica se o organizador atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass().equals(obj.getClass())) {
            return false;
        }
        Organizador o = (Organizador) obj;
        return this.utilizador.equals(o.utilizador);
    }

}
