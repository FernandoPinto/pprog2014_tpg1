/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * Classe que representa um revisor com nome, utilizador associado e lista de
 * tópicos de perícia.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Revisor implements Serializable {

    private String nome;
    private Utilizador utilizador;
    private List<Topico> topicosPericia;

    /**
     * Constrói uma instância de Revisor com nome e utilizador associado e
     * inicializa a lista de tópicos vazia.
     *
     * @param u utilizador a associar
     */
    public Revisor(Utilizador u) {
        nome = u.getNome();
        utilizador = u;
        topicosPericia = new ArrayList();
    }

    /**
     * Constrói uma instância de Revisor com as mesmas caracteristicas do
     * revisor passado por parâmetro.
     *
     * @param r revisor a copiar
     */
    public Revisor(Revisor r) {
        this.nome = r.utilizador.getNome();
        this.utilizador = new Utilizador(r.utilizador);
        this.topicosPericia = new ArrayList(r.topicosPericia);
    }

    /**
     * Constrói uma instância de Revisor sem informação.
     */
    public Revisor() {
    }

    /**
     * Devolve o nome do revisor
     *
     * @return nome do revisor
     */
    public String getNome() {
        return utilizador.getNome();
    }

    /**
     * Devolve o utilizador associado do revisor
     *
     * @return utilizador associado
     */
    public Utilizador getUtilizador() {
        return utilizador;
    }

    /**
     * Devolve a lista de tópicos de perícia do revisor.
     *
     * @return lista de tópicos de perícia
     */
    public List<Topico> getTopicosPericia() {
        return topicosPericia;
    }

    /**
     * Modifica o nome do revisor.
     *
     * @param nome novo nome do revisor
     */
    public void setNome(String nome) {
        this.nome = nome;
        utilizador.setNome(nome);
    }

    /**
     * Modifica o utilizador associado do revisor.
     *
     * @param utilizador novo utilizador associado
     */
    public void setUtilizador(Utilizador utilizador) {
        this.utilizador = utilizador;
        this.nome = utilizador.getNome();
    }

    /**
     * Modifica a lista de tópicos de perícia do revisor.
     *
     * @param listaTopicos nova lista de tópicos de perícia
     */
    public void setTopicosPericia(List<Topico> listaTopicos) {
        topicosPericia.addAll(listaTopicos);
    }

    /**
     * Devolve uma descrição textual do revisor.
     *
     * @return descrição do revisor
     */
    @Override
    public String toString() {
        return "Revisor: " + this.getNome();
    }

    /**
     * Verifica se o revisor atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Revisor r = (Revisor) obj;
        boolean bool = false;
        if (!(this.utilizador.equals(r.utilizador) && this.topicosPericia.size() == r.topicosPericia.size())) {
            return false;
        }
        for (Topico topico : topicosPericia) {
            for (Topico topico1 : r.topicosPericia) {
                if (topico.equals(topico1)) {
                    bool = true;
                    break;
                } else {
                    bool = false;
                }
            }
        }
        return bool;
    }

    /**
     * Adiciona um tópico à lista de tópicos de perícia do revisor.
     *
     * @param t tópico a adicionar
     */
    public void addTopico(Topico t) {
        this.topicosPericia.add(t);
    }
}
