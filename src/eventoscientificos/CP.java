package eventoscientificos;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * Classe que representa uma comissão de programa com uma lista de revisores.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class CP implements Serializable {

    private List<Revisor> revisores;

    /**
     * Constrói uma instância de CP com uma lista de revisores.
     *
     * @param revisores lista de revisores da CP
     */
    public CP(List<Revisor> revisores) {
        this.revisores = new ArrayList(revisores);
    }

    /**
     * Constrói uma instância de CP inicializando a lista de revisores vazia.
     */
    public CP() {
        revisores = new ArrayList();
    }

    /**
     * Constrói uma instância de CP com as mesmas características da cp passada
     * por parâmetro.
     *
     * @param cp cp a copiar
     */
    public CP(CP cp) {
        this.revisores = new ArrayList(cp.revisores);
    }

    /**
     * Devolve a lista de revisores da CP
     *
     * @return lista de revisores
     */
    public List<Revisor> getListaRevisores() {
        return this.revisores;
    }

    /**
     * Modifica a lista de revisores da comissão de programa.
     *
     * @param revisores nova lista de revisores da comissão de programa
     */
    public void setListaRevisores(List<Revisor> revisores) {
        this.revisores = new ArrayList(revisores);
    }

    /**
     * Devolve uma descrição textual da CP e dos seus revisores
     *
     * @return descrição da CP
     */
    @Override
    public String toString() {
        String aux = "Membros de CP: ";
        for (ListIterator<Revisor> it = revisores.listIterator(); it.hasNext();) {
            aux += it.next().toString();
            if (it.hasNext()) {
                aux += ", ";
            }
        }
        return aux;
    }

    /**
     * Verifica se a comissão de programa atual é igual ao objecto passado por
     * parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        CP cp = (CP) obj;
        if (!(this.revisores.size() == cp.revisores.size())) {
            return false;
        }
        boolean bool = false;
        for (Revisor r : revisores) {
            for (Revisor r1 : cp.revisores) {
                if (r.equals(r1)) {
                    bool = true;
                }
                bool = false;
            }
        }
        return bool;
    }

    /**
     * Adiciona um revisor à lista de revisores da CP.
     *
     * @param id identificação do revisor
     * @param u utilizador associado ao revisor
     * @return novo revisor
     */
    public Revisor addMembroCP(String id, Utilizador u) {
        return new Revisor(u);
    }

    /**
     * Adiciona um revisor à lista de revisores da CP
     *
     * @param r o revisor a ser adicionado
     * @return true se bem sucedido na adição do revisor, false se não
     */
    public boolean registaMembroCP(Revisor r) {
        return revisores.add(r);
    }
}
