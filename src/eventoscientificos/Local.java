/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa um local, com descrição textual.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Local implements Serializable {

    private String descricao;

    /**
     * Cria uma nova instância de Local com descrição.
     *
     * @param descricao descrição do local
     */
    public Local(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Constrói uma instância de Local sem informção
     */
    public Local() {
    }

    /**
     * Devolve a descrição do local.
     *
     * @return descrição do local
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Modifica a descrição do descricao.
     *
     * @param descricao nova descrição do local
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Devolve a descrição do local
     *
     * @return descrição do local
     */
    @Override
    public String toString() {
        return descricao;
    }

    /**
     * Verifica se o local atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass().equals(obj.getClass())) {
            return false;
        }
        Local l = (Local) obj;

        return this.descricao.equals(l.descricao);
    }

}
