/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa um tópico de evento, com descrição e código ACM.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Topico implements Serializable {

    private String descricao;
    private String codigo;

    /**
     * Constrói uma instância de tópico com descrição e código.
     *
     * @param descricao descrição do tópico
     * @param codigo código do tópico
     */
    public Topico(String descricao, String codigo) {
        this.descricao = descricao;
        this.codigo = codigo;
    }

    /**
     * Constrói uma instância de tópico sem informação
     */
    public Topico() {
    }

    /**
     * Constrói uma instância de Topico com as mesmas características que o
     * Tópico passado por parâmetro.
     *
     * @param t tópico a copiar
     */
    public Topico(Topico t) {
        this.descricao = t.descricao;
        this.codigo = t.codigo;
    }

    /**
     * Devolve a descrição do tópico.
     *
     * @return descrição do tópico
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve o código do tópico
     *
     * @return código do tópico
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Modifica a descrição do tópico.
     *
     * @param descricao nova descrição do tópico
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Modifica o código do tópico.
     *
     * @param codigo novo código do tópico
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Devolve uma descrição textual do tópico.
     *
     * @return descrição do tópico
     */
    @Override
    public String toString() {
        return "Codigo: " + this.codigo + "\nDescrição: " + this.descricao;
    }

    /**
     * Verifica se o tópico atual é igual ao objeto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Topico t = (Topico) obj;
        return this.codigo.equalsIgnoreCase(t.codigo);
    }

}
