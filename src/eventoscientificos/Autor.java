/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.io.Serializable;

/**
 * Classe que representa um autor de um artigo científico com nome, instituição
 * de afiliação, email, e {@link Utilizador} associado.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Autor implements Serializable {

    private Utilizador utilizador;
    private String nome;
    private String afiliacao;
    private String email;
    private static final String NOME_POR_OMISSAO = "Sem nome";
    private static final String AFILIACAO_POR_OMISSAO = "Sem afiliação";
    private static final String EMAIL_POR_OMISSAO = "Sem email";

    /**
     * Constrói uma instância de Autor com nome, email e instituição de
     * afiliação.
     *
     * @param nome nome do autor
     * @param email email do autor
     * @param afiliacao instituição de afiliação do autor
     */
    public Autor(String nome, String email, String afiliacao) {
        this.nome = nome;
        this.email = email;
        this.afiliacao = afiliacao;
    }

    /**
     * Constrói uma instância de Autor com as mesmas características do
     * utilizador recebido como parâmetro, e associa os dois.
     *
     * @param u utlizador a associar ao autor atual
     */
    public Autor(Utilizador u) {
        this.utilizador = u;
        this.nome = u.getNome();
        this.email = u.getEmail();
        this.afiliacao = Autor.AFILIACAO_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Autor iniciando as variáveis com os valores por
     * omissão.
     */
    public Autor() {
        this.nome = Autor.NOME_POR_OMISSAO;
        this.email = Autor.EMAIL_POR_OMISSAO;
        this.afiliacao = Autor.AFILIACAO_POR_OMISSAO;
    }

    /**
     * Constrói uma instância de Autor com as mesmas características do autor
     * passado por parâmetro.
     *
     * @param a autor a copiar
     */
    public Autor(Autor a) {
        if (a.utilizador != null) {
            this.utilizador = a.utilizador;
        }
        this.nome = a.nome;
        this.email = a.email;
        this.afiliacao = a.afiliacao;
    }

    /**
     * Devolve o utilizador associado do autor.
     *
     * @return utilizador associado do autor
     */
    public Utilizador getUtilizador() {
        return this.utilizador;
    }

    /**
     * Devolve o nome do autor.
     *
     * @return nome do autor
     */
    public String getNome() {
        return this.nome;
    }

    /**
     * Devolve a descrição textual da instituição de afiliação do autor.
     *
     * @return descrição da instituição de afilicação do autor
     */
    public String getAfiliacao() {
        return this.afiliacao;
    }

    /**
     * Devolve o email do autor.
     *
     * @return email do autor
     */
    public String getEmail() {
        return this.email;
    }

    /**
     * Modifica o utilizador associado ao autor.
     *
     * @param u novo utilizador a associar ao autor
     */
    public void setUtilizador(Utilizador u) {
        this.utilizador = u;
        this.nome = u.getNome();
        this.email = u.getEmail();
    }

    /**
     * Modifica o nome do autor.
     *
     * @param nome novo nome do autor
     */
    public void setNome(String nome) {
        if (this.utilizador != null) {
            this.utilizador.setNome(nome);
        }
        this.nome = nome;
    }

    /**
     * Modifica a instituição de afiliação do autor.
     *
     * @param afiliacao nova instituição de afiliação do autor
     */
    public void setAfiliacao(String afiliacao) {
        this.afiliacao = afiliacao;
    }

    /**
     * Modifica o email do autor.
     *
     * @param email novo email do autor
     */
    public void setEMail(String email) {
        if (this.utilizador != null) {
            this.utilizador.setEmail(email);
        }
        this.email = email;
    }

    /**
     * Devolve uma descrição textual do autor.
     *
     * @return descrição do autor
     */
    @Override
    public String toString() {
        return String.format("%s - %s - %s", nome, afiliacao, email);
    }

    /**
     * Verifica se o autor atual é igual ao objecto passado por parâmetro.
     *
     * @param obj objeto de comparação
     * @return true se forem iguais, false se não
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        Autor a = (Autor) obj;
        if (this.utilizador != null && a.utilizador != null) {
            return this.utilizador.equals(a.utilizador);
        }
        return this.email.equals(a.email);
    }

    /**
     * Verifica se o autor atual pode ser autor corresponde de um artigo.
     *
     * @return true se for possível, false se não
     */
    public boolean podeSerCorrespondente() {
        return (utilizador != null);
    }
}
