/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import eventoscientificos.Artigo;
import eventoscientificos.Autor;
import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import eventoscientificos.Local;
import eventoscientificos.Organizador;
import eventoscientificos.Revisor;
import eventoscientificos.Submissao;
import eventoscientificos.Topico;
import eventoscientificos.Utilizador;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 * Classe de auxílio à leitura e gravação de dados sobre a empresa.
 *
 * @author Fernando Pinto, Renato Ayres
 */
public class Utils {

    private static List<Utilizador> utilizadores;
    private static List<Autor> autores;
    private static List<Organizador> organizadores;
    private static List<Topico> topicos;
    private static List<Submissao> submissoes;
    private static List<Evento> eventos;
    private static List<Revisor> revisores;

    /**
     * Este metodo lê utilizadores de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro que vai ser lido
     * @return lista de utilizadores
     * @throws FileNotFoundException
     */
    static public List<Utilizador> readUtilizadoresFromText(String ficheiro) throws FileNotFoundException {
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            Utilizador u = new Utilizador(linha.split(";")[0], linha.split(";")[1], linha.split(";")[2], linha.split(";")[3]);
            if (!(utilizadores.contains(u))) {
                utilizadores.add(u);
            }
        }

        return utilizadores;
    }

    /**
     * Este metodo lê autores de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de autores
     * @throws FileNotFoundException
     */
    static public List<Autor> readAutoresFromText(String ficheiro) throws FileNotFoundException {
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            if (utilizadores == null) {
                throw new NullPointerException();
            }
            for (Utilizador utilizador : utilizadores) {
                if (linha.equals(utilizador.getEmail())) {
                    Autor a = new Autor(utilizador);
                    if (!(autores.contains(a))) {
                        autores.add(a);
                        break;
                    }
                }
            }
        }

        return autores;
    }

    /**
     * Este metodo lê organizadores de um ficheiro de Texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de organizadores
     * @throws FileNotFoundException
     */
    static public List<Organizador> readOrganizadoresFromText(String ficheiro) throws FileNotFoundException {
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            if (utilizadores == null) {
                throw new NullPointerException();
            }
            for (Utilizador utilizador : utilizadores) {
                if (linha.equals(utilizador.getEmail())) {
                    Organizador o = new Organizador(utilizador);
                    if (!(organizadores.contains(o))) {
                        organizadores.add(o);
                    }
                }
            }
        }
        return organizadores;
    }

    /**
     * Este metodo lê revisores de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de revisores
     * @throws FileNotFoundException
     */
    static public List<Revisor> readRevisoresFromText(String ficheiro) throws FileNotFoundException {
        String[] codigoTopicos;
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            codigoTopicos = linha.split(";")[1].split("_");
            if (utilizadores == null) {
                throw new NullPointerException();
            }
            for (Utilizador utilizador : utilizadores) {
                if (linha.split(";")[0].equals(utilizador.getEmail())) {
                    Revisor r = new Revisor(utilizador);
                    for (String codigoTopico : codigoTopicos) {
                        for (Topico topico : topicos) {
                            if (codigoTopico.equals(topico.getCodigo())) {
                                if (!(r.getTopicosPericia().contains(topico))) {
                                    r.addTopico(topico);
                                    break;
                                }
                            }
                        }
                    }
                    if (!(revisores.contains(r))) {
                        revisores.add(r);
                    }
                }
            }
        }

        return revisores;
    }

    /**
     * Este metodo lê topicos de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de topicos
     * @throws FileNotFoundException
     */
    static public List<Topico> readTopicosFromText(String ficheiro) throws FileNotFoundException {
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            Topico t = new Topico(linha.split(";")[0], linha.split(";")[1]);
            if (!(topicos.contains(t))) {
                topicos.add(t);
            }
        }
        return topicos;
    }

    /**
     * Este metodo lê artigos de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de artigos
     * @throws FileNotFoundException
     */
    static public List<Submissao> readArtigosFromText(String ficheiro) throws FileNotFoundException {
        String[] codigoTopicos;
        String[] emailAutores;
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            emailAutores = linha.split(";")[2].split("_");
            codigoTopicos = linha.split(";")[3].split("_");
            Artigo a = new Artigo(linha.split(";")[0], linha.split(";")[1]);
            for (String emailAutor : emailAutores) {
                for (Autor autor : autores) {
                    if (emailAutor.equals(autor.getEmail())) {
                        if (!(a.getAutores().contains(autor))) {
                            a.addAutor(autor);
                            break;
                        }
                    }
                }
            }
            for (String codigoTopico : codigoTopicos) {
                for (Topico topico : topicos) {
                    if (codigoTopico.equals(topico.getCodigo())) {
                        if (!(a.getTopicos().contains(topico))) {
                            a.addTopico(topico);
                            break;
                        }
                    }
                }
            }

            Submissao s = new Submissao();
            s.setArtigo(a);
            submissoes.add(s);

            for (Evento evento : eventos) {
                if (linha.split(";")[4].equals(evento.getTitulo())) {
                    if (!(evento.getSubmissoes().contains(s))) {
                        evento.addSubmissao(s);
                        break;
                    }
                }
            }
        }
        return submissoes;
    }

    /**
     * Este metodo lê Eventos de um ficheiro de texto
     *
     * @param ficheiro nome do ficheiro de texto
     * @return lista de eventos
     * @throws FileNotFoundException
     */
    static public List<Evento> readEventoFromText(String ficheiro) throws FileNotFoundException {
        File f = new File(ficheiro);
        Scanner scan = new Scanner(f);
        while (scan.hasNextLine()) {
            String linha = scan.nextLine();
            String[] codigoTopicos;
            String[] emailOrganizadores;
            String[] emailRevisores;
            Evento e = new Evento(linha.split(";")[0], linha.split(";")[1]);
            e.setLocal(new Local(linha.split(";")[2]));
            codigoTopicos = linha.split(";")[3].split("_");
            emailOrganizadores = linha.split(";")[4].split("_");
            emailRevisores = linha.split(";")[5].split("_");
            for (String codigoTopico : codigoTopicos) {
                for (Topico topico : topicos) {
                    if (codigoTopico.equals(topico.getCodigo())) {
                        if (!(e.getTopicos().contains(topico))) {
                            e.addTopico(topico);
                            break;
                        }
                    }
                }
            }

            for (String emailOrganizador : emailOrganizadores) {
                for (Organizador organizador : organizadores) {
                    if (emailOrganizador.equals(organizador.getUtilizador().getEmail())) {
                        if (!(e.getOrganizadores().contains(organizador))) {
                            e.addOrganizador(organizador);
                            break;
                        }
                    }
                }
            }

            for (String emailRevisor : emailRevisores) {
                for (Revisor revisor : revisores) {
                    if (emailRevisor.equals(revisor.getUtilizador().getEmail())) {
                        if (!(e.getCp().getListaRevisores().contains(revisor))) {
                            e.addRevisor(revisor);
                            break;
                        }
                    }
                }
            }
            if (!(eventos.contains(e)))  {
                eventos.add(e);
            }

        }

        return eventos;
    }

    /**
     * Lê toda a informação de ficheiros de texto predefinidos e adiciona à
     * empresa.
     *
     * @param empresa empresa para trabalhar
     * @throws FileNotFoundException
     */
    public static void readInformacao(Empresa empresa) throws FileNotFoundException {
        utilizadores = empresa.getUtilizadores();
        organizadores = empresa.getOrganizadores();
        revisores = empresa.getRevisores();
        topicos = empresa.getTopicos();
        autores = empresa.getAutores();
        submissoes = empresa.getSubmissoes();
        eventos = (ArrayList<Evento>) empresa.getEventos();
        Utils.readUtilizadoresFromText("Utilizadores.txt");
        Utils.readAutoresFromText("Autores.txt");
        Utils.readTopicosFromText("Topicos.txt");
        Utils.readRevisoresFromText("Revisores.txt");
        Utils.readOrganizadoresFromText("Organizadores.txt");
        Utils.readEventoFromText("Eventos.txt");
        Utils.readArtigosFromText("Artigos.txt");
    }

    /**
     * Grava uma empresa num ficheiro com o nome empresa.bin
     *
     * @param e
     * @throws IOException
     */
    public static void gravarEmpresaBinario(Empresa e) throws IOException {

        JFileChooser fc = new JFileChooser();
        fc.showSaveDialog(null);
        ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fc.getSelectedFile()));
        out.writeObject(e);

    }

    /**
     * Devolve uma empresa lida de um ficheiro com o nome empresa.bin situado no
     * diretório do projeto.
     *
     * @return empresa carregada
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Empresa carregarEmpresaBinario() throws IOException, ClassNotFoundException {

        ObjectInputStream in = new ObjectInputStream(new FileInputStream("empresa.bin"));

        Empresa e = (Empresa) in.readObject();

        return e;
    }

    /**
     * Devolve uma empresa guardada de forma persistente num ficheiro a ser
     * escolhido pelo utilizador.
     *
     * @param janela janela de trabalho
     * @return nova empresa de trabalho
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static Empresa procurarEmpresaBinario(JFrame janela) throws IOException, ClassNotFoundException {
        Empresa e;
        JFileChooser chooser = new JFileChooser();
        int returnVal = chooser.showOpenDialog(janela);
        File f = chooser.getSelectedFile();
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(f));

        e = (Empresa) in.readObject();
        return e;
    }

}
