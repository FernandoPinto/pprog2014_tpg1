/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificosUI;

import eventoscientificos.Empresa;
import eventoscientificos.Evento;
import eventoscientificos.MecanismoDistribuicao;
import eventoscientificos.Organizador;
import eventoscientificos.ProcessoDistribuicao;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import utils.Utils;

/**
 *
 * @author fernando
 */
public class JanelaPrincipal extends JFrame {

    private Empresa empresa;
    private Organizador org;
    private JMenuBar mb;
    private JMenu menuFile, menuWatch;
    private JMenuItem definirOrganizador, carregarFicheirosTexto, carregarEmpresaBinario, guardarFicheiros, itemSair, verDistribuicoes, distribuir;
    private JLabel nomeEmpresa;

    public JanelaPrincipal(Empresa e) {
        super();

        this.empresa = e;

        setJMenuBar(criarBarraMenu());

        super.setTitle("Eventos Cientificos");
        super.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        super.setSize(400, 300);
        super.setLocationRelativeTo(null);
        super.setLayout(new BorderLayout());

        super.add(criarNomeEmpresa(), BorderLayout.NORTH);

        super.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                sair();
            }
        });

        super.setVisible(true);

    }

    private JMenuBar criarBarraMenu() {
        mb = new JMenuBar();
        mb.add(criarMenuFile());
        mb.add(criarMenuDistrib());
        return mb;
    }

    private JMenu criarMenuDistrib() {
        menuWatch = new JMenu("Distribuições");
        menuWatch.add(criarItemDistribuirArtigos());
        menuWatch.add(criarItemVerDistribuições());
        return menuWatch;
    }

    private JMenuItem criarItemVerDistribuições() {
        verDistribuicoes = new JMenuItem("Ver distribuições");
        verDistribuicoes.setMnemonic('v');
        verDistribuicoes.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
                    List<Evento> list = empresa.getEventosDistribuidos(empresa.getEventosOrganizador(org.getUtilizador().getUsername()));
                    String op[] = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        op[i] = list.get(i).getTitulo();
                    }
                    String aux = (String) JOptionPane.showInputDialog(JanelaPrincipal.this, "Escolha o Evento:", "Ver distribuições", -1, null, op, op[0]);
                    Evento evento = empresa.getEvento(aux);
                    JList listaDistribuicoes = new JList(evento.getProcessoDistribuicao().distribui().toArray());
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, listaDistribuicoes, String.format("Distribuição do evento ", evento.getTitulo()), 1, null);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não há eventos distribuídos. Vá a Distribuições -> Distribuir artigos para começar a trabalhar!", "Ver distribuições", 2, null);
                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não foi definido organizador. Vá a Ficheiro -> Definir Organizador para começar a trabalhar.", "ERRO", 0);
                }
            }
        }
        );
        return verDistribuicoes;
    }

    private JMenuItem criarItemDistribuirArtigos() {
        distribuir = new JMenuItem("Distribuir Artigos");
        distribuir.setMnemonic('d');
        distribuir.addActionListener(new ActionListener() {

            private Evento event;

            public void actionPerformed(ActionEvent ae) {
                try {
                    event = escolherEvento();
                    distribuir(event);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não existem eventos nestas condições", "ERRO", 0);
                } catch (NullPointerException ex) {
                } catch (SemOrganizadorException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não foi definido organizador. Vá a Ficheiro -> Definir Organizador para começar a trabalhar.", "ERRO", 0);
                }
            }
        });
        return distribuir;
    }

    private JMenu criarMenuFile() {
        menuFile = new JMenu("Ficheiros");
        menuFile.add(criarItemDefinirOrganizador());
        menuFile.add(criarItemCarregarFicheiros());
        menuFile.add(criarItemCarregarEmpresaBinario());
        menuFile.add(criarItemGuardarFicheiros());
        menuFile.addSeparator();
        menuFile.add(criarItemSair());
        return menuFile;
    }

    private JMenuItem criarItemCarregarFicheiros() {
        carregarFicheirosTexto = new JMenuItem("Carregar dados(ficheiro de texto)");
        carregarFicheirosTexto.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK));
        carregarFicheirosTexto.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                try {

                    Utils.readInformacao(empresa);

                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Informações carregadas com sucesso", "Sucesso!", 1);

                } catch (FileNotFoundException ex) {
                    Logger.getLogger(JanelaPrincipal.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        return carregarFicheirosTexto;
    }

    private JMenuItem criarItemGuardarFicheiros() {
        guardarFicheiros = new JMenuItem("Guardar ficheiros");
        guardarFicheiros.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK));
        guardarFicheiros.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                JFileChooser fc = new JFileChooser();
                fc.showSaveDialog(null);
                try {
                    ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(fc.getSelectedFile()));
                    out.writeObject(empresa);

                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Erro inesperado, impossível gravar ficheiro.", "ERRO", 0);
                } catch (NullPointerException ex) {

                }
            }
        });
        return guardarFicheiros;
    }

    private JMenuItem criarItemSair() {
        itemSair = new JMenuItem("Sair");
        itemSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
        itemSair.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                sair();
            }

        });
        return itemSair;
    }

    private JPanel criarNomeEmpresa() {
        JPanel painel = new JPanel();
        nomeEmpresa = new JLabel(empresa.getNome() + " - Organizadora de eventos.");
        painel.add(nomeEmpresa);
        return painel;
    }

    private void sair() {
        try {
            int op = JOptionPane.showConfirmDialog(null, "Deseja guardar dados ?", "Sair", 1);
            if (op == JOptionPane.YES_OPTION) {
                try {

                    Utils.gravarEmpresaBinario(empresa);
                    System.exit(0);
                } catch (IOException ex) {
                    Logger.getLogger(JanelaPrincipal.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            } else if (op == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        } catch (NullPointerException ex) {
        }
    }

    private void distribuir(Evento event) {

        JComboBox box;
        MecanismoDistribuicao mecanismo;

        try {
            box = makeBoxMecanismos();
            int resultMecanismo = escolherMecanismo(box);

            if (resultMecanismo == 0) {
                mecanismo = empresa.getMecanismos().get(box.getSelectedIndex());
                ProcessoDistribuicao pd = event.novoProcessoDistribuicao(mecanismo);
                mecanismo.openCriterios(pd);
            } else if (resultMecanismo == 2) {
                mecanismo = empresa.getMecanismos().get(box.getSelectedIndex());
                JOptionPane.showMessageDialog(JanelaPrincipal.this, mecanismo.toString(), "Ajuda", 2);
                distribuir(event);
            }
        } catch (ArrayIndexOutOfBoundsException ex) {
            JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não há eventos!", "Distribuir", 2);
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(this, "Impossível distribuir revisões nas condições especificadas", "ERRO", 0);
        }
    }

    private Evento escolherEvento() throws SemOrganizadorException {

        if (org == null) {
            throw new SemOrganizadorException();
        }

        String opEvento[] = new String[empresa.getEventosOrganizador(org.getUtilizador().getUsername()).size()];
        System.out.println(opEvento.length);
        int i = 0;
        for (Evento e : empresa.getEventosOrganizador(org.getUtilizador().getUsername())) {
            opEvento[i] = e.getTitulo();
            i++;
        }
        String resultEvento = (String) JOptionPane.showInputDialog(JanelaPrincipal.this, "Escolha o Evento:", "Distribuir", -1, null, opEvento, opEvento[0]);
        return empresa.getEvento(resultEvento);
    }

    private int escolherMecanismo(JComboBox box) {
        String opcoes[] = {"OK", "Cancelar", "Ajuda"};
        return JOptionPane.showOptionDialog(JanelaPrincipal.this, box, "Escolha o Mecanismo", 0, 3, null, opcoes, opcoes[0]);
    }

    private JComboBox makeBoxMecanismos() {
        JComboBox box;
        String opMecanismo[] = new String[empresa.getMecanismos().size()];
        int j = 0;
        for (MecanismoDistribuicao m : empresa.getMecanismos()) {
            opMecanismo[j] = m.getName();
            j++;
        }
        box = new JComboBox(opMecanismo);
        return box;
    }

    private JMenuItem criarItemDefinirOrganizador() {
        definirOrganizador = new JMenuItem("Definir Organizador");
        definirOrganizador.setMnemonic('o');
        definirOrganizador.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                try {
                    List<Organizador> list = empresa.getOrganizadores();
                    String op[] = new String[list.size()];
                    for (int i = 0; i < list.size(); i++) {
                        op[i] = list.get(i).getUtilizador().getUsername();
                    }
                    String aux = (String) JOptionPane.showInputDialog(JanelaPrincipal.this, "Escolha o Organizador:", "Definir organizador", -1, null, op, op[0]);
                    for (Organizador o : list) {
                        if (aux.equals(o.getUtilizador().getUsername())) {
                            org = o;
                            break;
                        }
                    }
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, String.format("Organizador definido para: " + org.getNome()), "Definir organizador", 3);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Não há organizadores! Vá a Ficheiro -> Carregar dados para começar a trabalhar!", "ERRO", 0);
                } catch (NullPointerException ex) {

                }
            }
        });
        return definirOrganizador;
    }

    private JMenuItem criarItemCarregarEmpresaBinario() {
        carregarEmpresaBinario = new JMenuItem("Carregar Ficheiro de Dados");
        carregarEmpresaBinario.setMnemonic('e');
        carregarEmpresaBinario.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                try {
                    empresa = Utils.procurarEmpresaBinario(JanelaPrincipal.this);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Ficheiro inválido!", "ERRO", 0);
                } catch (ClassNotFoundException ex) {
                    JOptionPane.showMessageDialog(JanelaPrincipal.this, "Informação inválida no ficheiro!", "ERRO", 0);
                } catch (NullPointerException ex) {
//
                }

            }
        });
        return carregarEmpresaBinario;
    }
}
