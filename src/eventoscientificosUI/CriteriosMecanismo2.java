/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificosUI;

import eventoscientificos.Evento;
import eventoscientificos.ProcessoDistribuicao;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author Renato Ayres
 */
public class CriteriosMecanismo2 extends JDialog implements Serializable {

    private JPanel centro, sul, linhaRevisores, linhaRevisoresAfinidade, linhaTopicosAfinidade;
    private JLabel revisores, revisoresAfinidade, topicosAfinidade;
    private JTextField inRevisores, inRevisoresAfinidade, inTopicosAfinidade;
    private JButton jbOK, jbCancelar;

    private final ProcessoDistribuicao pd;

    public CriteriosMecanismo2(ProcessoDistribuicao processo) {
        super();

        pd = processo;

        setTitle("Critérios");
        setModal(true);

        add(criarPainelCentro());
        add(criarPainelSul(), BorderLayout.SOUTH);

        getRootPane().setDefaultButton(jbOK);

        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private JPanel criarPainelCentro() {
        centro = new JPanel(new GridLayout(3, 1));
        centro.add(criarLinhaRevisores());
        centro.add(criarLinhaRevisoresAfinidade());
        centro.add(criarLinhaTopicosAfinidade());
        return centro;
    }

    private JPanel criarLinhaRevisores() {
        linhaRevisores = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        revisores = new JLabel("Revisores (número mínimo):");
        revisores.setHorizontalAlignment(JLabel.RIGHT);
        linhaRevisores.add(revisores);

        inRevisores = new JTextField(String.valueOf(pd.getRevisoresArtigo()), 5);
        inRevisores.setEditable(false);
        inRevisores.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                inRevisores.setEditable(true);
            }
        });
        linhaRevisores.add(inRevisores);

        return linhaRevisores;
    }

    private JPanel criarLinhaRevisoresAfinidade() {
        linhaRevisoresAfinidade = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        revisoresAfinidade = new JLabel("Revisores com afinidade (número mínimo):");
        revisoresAfinidade.setHorizontalAlignment(JLabel.RIGHT);
        linhaRevisoresAfinidade.add(revisoresAfinidade);

        inRevisoresAfinidade = new JTextField(String.valueOf(pd.getRevisoresAfinidade()), 5);
        inRevisoresAfinidade.setEditable(false);
        inRevisoresAfinidade.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                inRevisoresAfinidade.setEditable(true);
            }
        });
        linhaRevisoresAfinidade.add(inRevisoresAfinidade);

        return linhaRevisoresAfinidade;
    }

    private JPanel criarLinhaTopicosAfinidade() {
        linhaTopicosAfinidade = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        topicosAfinidade = new JLabel("Afinidade (mínimo tópicos):");
        topicosAfinidade.setHorizontalAlignment(JLabel.RIGHT);
        linhaTopicosAfinidade.add(topicosAfinidade);

        inTopicosAfinidade = new JTextField(String.valueOf(pd.getTopicosAfinidade()), 5);
        inTopicosAfinidade.setEditable(false);
        inTopicosAfinidade.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                inTopicosAfinidade.setEditable(true);
            }
        });
        linhaTopicosAfinidade.add(inTopicosAfinidade);

        return linhaTopicosAfinidade;
    }

    private JPanel criarPainelSul() {
        sul = new JPanel();
        sul.add(botaoOK());
        sul.add(botaoCancelar());
        return sul;
    }

    private JButton botaoOK() {
        jbOK = new JButton("OK");
        jbOK.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                int revisores, revisoresAfinidade, topicosAfinidade;
                try {
                    revisores = Integer.parseInt(inRevisores.getText());
                    revisoresAfinidade = Integer.parseInt(inRevisoresAfinidade.getText());
                    topicosAfinidade = Integer.parseInt(inTopicosAfinidade.getText());
                    if (revisores < 1 || revisoresAfinidade < 0 || topicosAfinidade < 0) {
                        throw new NumberFormatException();
                    }
                    if (revisores > pd.getEvento().getCp().getListaRevisores().size() || topicosAfinidade > pd.getEvento().getTopicos().size() || revisoresAfinidade > revisores) {
                        throw new ArrayIndexOutOfBoundsException();
                    }
                    pd.setRevisoresArtigo(revisores);
                    pd.setRevisoresAfinidade(revisoresAfinidade);
                    pd.setTopicosAfinidade(topicosAfinidade);

                    dispose();
                    int resultado = confirmaDistribuicao(pd);
                    if (resultado == 0) {
                        pd.getEvento().setProcessoDistribuicao(pd);
                        JOptionPane.showMessageDialog(CriteriosMecanismo2.this, "Distribuição efetuada!", "SUCESSO!", 1);
                    } else {
                        pd.getMecanismo().openCriterios(pd);
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(CriteriosMecanismo2.this, "Valores incorretos nos revisores ou nos tópicos!", "ERRO", 0);
                } catch (ArrayIndexOutOfBoundsException ex) {
                    JOptionPane.showMessageDialog(CriteriosMecanismo2.this, String.format("Valores dos revisores ou dos tópicos ultrapassam o máximo! (%d para os revisores, %d para os tópicos, revisores para os revisores com afinidade)", pd.getEvento().getCp().getListaRevisores().size(), pd.getEvento().getTopicos().size()), "ERRO", 0);
                }
            }
        }
        );
        return jbOK;
    }

    private JButton botaoCancelar() {
        jbCancelar = new JButton("Cancelar");
        jbCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return jbCancelar;
    }

    private int confirmaDistribuicao(ProcessoDistribuicao pd) {
        JList listaDistribuicoes = new JList(pd.distribui().toArray());
        String op[] = {"Confirmar", "Cancelar"};
        return JOptionPane.showOptionDialog(this, listaDistribuicoes, String.format("Confirma distribuição para o evento %s?", pd.getEvento().getTitulo()), 0, 1, null, op, op[0]);
    }
}
