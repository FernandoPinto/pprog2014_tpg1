/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Renato Ayres
 */
public class MecanismoDistribuicao2Test {

    public MecanismoDistribuicao2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of distribui method, of class MecanismoDistribuicao2.
     */
    @Test
    public void testDistribui() {
        System.out.println("distribui");
        Submissao suba = new Submissao();
        Artigo arta = new Artigo("a", "b");
        arta.addTopico(new Topico("", "UM1111"));
        arta.addTopico(new Topico("", "DOIS22"));
        arta.addTopico(new Topico("", "TRES33"));
        arta.addTopico(new Topico("", "QUATRO"));
        suba.setArtigo(arta);

        Submissao subb = new Submissao();
        Artigo artb = new Artigo("c", "d");
        artb.addTopico(new Topico("", "CINCO5"));
        artb.addTopico(new Topico("", "SEIS66"));
        artb.addTopico(new Topico("", "SETE77"));
        artb.addTopico(new Topico("", "OITO88"));
        subb.setArtigo(artb);

        Submissao subc = new Submissao();
        Artigo artc = new Artigo("e", "f");
        artc.addTopico(new Topico("", "DOIS22"));
        artc.addTopico(new Topico("", "QUATRO"));
        artc.addTopico(new Topico("", "SEIS66"));
        artc.addTopico(new Topico("", "OITO88"));
        subc.setArtigo(artc);

        Submissao subd = new Submissao();
        Artigo artd = new Artigo("g", "h");
        artd.addTopico(new Topico("", "UM1111"));
        artd.addTopico(new Topico("", "TRES33"));
        artd.addTopico(new Topico("", "SETE77"));
        artd.addTopico(new Topico("", "NOVE99"));
        subd.setArtigo(artd);

        List<Submissao> submissoes = new ArrayList();
        submissoes.add(suba);
        submissoes.add(subb);
        submissoes.add(subc);
        submissoes.add(subd);

        List<Topico> tpa = new ArrayList();
        tpa.add(new Topico("", "DOIS22"));
        tpa.add(new Topico("", "CINCO5"));
        tpa.add(new Topico("", "SEIS66"));
        tpa.add(new Topico("", "OITO88"));

        List<Topico> tpb = new ArrayList();
        tpb.add(new Topico("", "UM1111"));
        tpb.add(new Topico("", "DOIS22"));
        tpb.add(new Topico("", "OITO88"));
        tpb.add(new Topico("", "NOVE99"));

        List<Topico> tpc = new ArrayList();
        tpc.add(new Topico("", "UM1111"));
        tpc.add(new Topico("", "DOIS22"));
        tpc.add(new Topico("", "TRES33"));
        tpc.add(new Topico("", "QUATRO"));

        List<Topico> tpd = new ArrayList();
        tpd.add(new Topico("", "UM1111"));
        tpd.add(new Topico("", "TRES33"));
        tpd.add(new Topico("", "CINCO5"));
        tpd.add(new Topico("", "SETE77"));
        tpd.add(new Topico("", "NOVE99"));

        List<Topico> tpe = new ArrayList();
        tpe.add(new Topico("", "DOIS22"));
        tpe.add(new Topico("", "TRES33"));
        tpe.add(new Topico("", "QUATRO"));
        tpe.add(new Topico("", "SEIS66"));
        tpe.add(new Topico("", "OITO88"));

        List<Revisor> revisores = new ArrayList();

        Revisor ra = new Revisor(new Utilizador("Carol", "123456", "Carol", "carol@isep.ipp.pt"));
        ra.setTopicosPericia(tpa);
        Revisor rb = new Revisor(new Utilizador("Nando", "123456", "Fernado", "fernando@isep.ipp.pt"));
        rb.setTopicosPericia(tpb);
        Revisor rc = new Revisor(new Utilizador("Marco", "123456", "Marco", "marco@isep.ipp.pt"));
        rc.setTopicosPericia(tpc);
        Revisor rd = new Revisor(new Utilizador("Renato", "123456", "Renato", "renato@isep.ipp.pt"));
        rd.setTopicosPericia(tpd);
        Revisor re = new Revisor(new Utilizador("Justino", "123456", "João", "joao@isep.ipp.pt"));
        re.setTopicosPericia(tpe);

        revisores.add(ra);
        revisores.add(rb);
        revisores.add(rc);
        revisores.add(rd);
        revisores.add(re);

        CP cp = new CP();
        cp.setListaRevisores(revisores);

        Evento e = new Evento();
        e.setCp(cp);
        e.setSubmissoes(submissoes);

        ProcessoDistribuicao pd = new ProcessoDistribuicao(e, new MecanismoDistribuicao1());
        pd.setRevisoresAfinidade(1);
        pd.setRevisoresArtigo(2);
        pd.setTopicosAfinidade(1);
        MecanismoDistribuicao2 instance = new MecanismoDistribuicao2();

        List<Distribuicao> expResult = new ArrayList();

        List<Revisor> da = new ArrayList();
        da.add(ra);
        da.add(rb);

        List<Revisor> db = new ArrayList();
        db.add(rd);
        db.add(rc);

        List<Revisor> dc = new ArrayList();
        dc.add(re);
        dc.add(rd);

        List<Revisor> dd = new ArrayList();
        dd.add(rb);
        dd.add(ra);

        expResult.add(new Distribuicao(arta, da));
        expResult.add(new Distribuicao(artb, db));
        expResult.add(new Distribuicao(artc, dc));
        expResult.add(new Distribuicao(artd, dd));
        List<Distribuicao> result = instance.distribui(pd);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisoresAfinidadeArtigo method, of class
     * MecanismoDistribuicao2.
     */
    @Test
    public void testGetRevisoresAfinidadeArtigo() {
        System.out.println("getRevisoresAfinidadeArtigo");
        int minimoTopicosAfinidade = 1;
        Submissao s = new Submissao();
        Artigo artb = new Artigo("c", "d");
        artb.addTopico(new Topico("", "CINCO5"));
        artb.addTopico(new Topico("", "SEIS66"));
        artb.addTopico(new Topico("", "SETE77"));
        artb.addTopico(new Topico("", "OITO88"));
        s.setArtigo(artb);

        List<Topico> tpa = new ArrayList();
        tpa.add(new Topico("", "DOIS22"));
        tpa.add(new Topico("", "CINCO5"));
        tpa.add(new Topico("", "SEIS66"));
        tpa.add(new Topico("", "OITO88"));

        List<Topico> tpb = new ArrayList();
        tpb.add(new Topico("", "UM1111"));
        tpb.add(new Topico("", "DOIS22"));
        tpb.add(new Topico("", "OITO88"));
        tpb.add(new Topico("", "NOVE99"));

        List<Topico> tpc = new ArrayList();
        tpc.add(new Topico("", "UM1111"));
        tpc.add(new Topico("", "DOIS22"));
        tpc.add(new Topico("", "TRES33"));
        tpc.add(new Topico("", "QUATRO"));

        List<Topico> tpd = new ArrayList();
        tpd.add(new Topico("", "UM1111"));
        tpd.add(new Topico("", "TRES33"));
        tpd.add(new Topico("", "CINCO5"));
        tpd.add(new Topico("", "SETE77"));
        tpd.add(new Topico("", "NOVE99"));

        List<Topico> tpe = new ArrayList();
        tpe.add(new Topico("", "DOIS22"));
        tpe.add(new Topico("", "TRES33"));
        tpe.add(new Topico("", "QUATRO"));
        tpe.add(new Topico("", "SEIS66"));
        tpe.add(new Topico("", "OITO88"));

        List<Revisor> revisores = new ArrayList();

        Revisor ra = new Revisor(new Utilizador("Carol", "123456", "Carol", "carol@isep.ipp.pt"));
        ra.setTopicosPericia(tpa);
        Revisor rb = new Revisor(new Utilizador("Nando", "123456", "Fernado", "fernando@isep.ipp.pt"));
        rb.setTopicosPericia(tpb);
        Revisor rc = new Revisor(new Utilizador("Marco", "123456", "Marco", "marco@isep.ipp.pt"));
        rc.setTopicosPericia(tpc);
        Revisor rd = new Revisor(new Utilizador("Renato", "123456", "Renato", "renato@isep.ipp.pt"));
        rd.setTopicosPericia(tpd);
        Revisor re = new Revisor(new Utilizador("Justino", "123456", "João", "joao@isep.ipp.pt"));
        re.setTopicosPericia(tpe);

        revisores.add(ra);
        revisores.add(rb);
        revisores.add(rd);
        revisores.add(re);

        Evento e = new Evento();
        CP cp = new CP();
        cp.setListaRevisores(revisores);
        e.setCp(cp);

        MecanismoDistribuicao2 instance = new MecanismoDistribuicao2();
        List<Revisor> expResult = revisores;
        List<Revisor> result = instance.getRevisoresAfinidadeArtigo(minimoTopicosAfinidade, artb, revisores);
        assertEquals(expResult, result);
    }

    /**
     * Test of getRevisoresRestantes method, of class MecanismoDistribuicao2.
     */
    @Test
    public void testGetRevisoresRestantes() {
        System.out.println("getRevisoresRestantes");
        int minimoTopicosAfinidade = 1;
        Submissao s = new Submissao();
        Artigo artb = new Artigo("c", "d");
        artb.addTopico(new Topico("", "CINCO5"));
        artb.addTopico(new Topico("", "SEIS66"));
        artb.addTopico(new Topico("", "SETE77"));
        artb.addTopico(new Topico("", "OITO88"));
        s.setArtigo(artb);

        List<Topico> tpa = new ArrayList();
        tpa.add(new Topico("", "DOIS22"));
        tpa.add(new Topico("", "CINCO5"));
        tpa.add(new Topico("", "SEIS66"));
        tpa.add(new Topico("", "OITO88"));

        List<Topico> tpb = new ArrayList();
        tpb.add(new Topico("", "UM1111"));
        tpb.add(new Topico("", "DOIS22"));
        tpb.add(new Topico("", "OITO88"));
        tpb.add(new Topico("", "NOVE99"));

        List<Topico> tpc = new ArrayList();
        tpc.add(new Topico("", "UM1111"));
        tpc.add(new Topico("", "DOIS22"));
        tpc.add(new Topico("", "TRES33"));
        tpc.add(new Topico("", "QUATRO"));

        List<Topico> tpd = new ArrayList();
        tpd.add(new Topico("", "UM1111"));
        tpd.add(new Topico("", "TRES33"));
        tpd.add(new Topico("", "CINCO5"));
        tpd.add(new Topico("", "SETE77"));
        tpd.add(new Topico("", "NOVE99"));

        List<Topico> tpe = new ArrayList();
        tpe.add(new Topico("", "DOIS22"));
        tpe.add(new Topico("", "TRES33"));
        tpe.add(new Topico("", "QUATRO"));
        tpe.add(new Topico("", "SEIS66"));
        tpe.add(new Topico("", "OITO88"));

        List<Revisor> revisores = new ArrayList();

        Revisor ra = new Revisor(new Utilizador("Carol", "123456", "Carol", "carol@isep.ipp.pt"));
        ra.setTopicosPericia(tpa);
        Revisor rb = new Revisor(new Utilizador("Nando", "123456", "Fernado", "fernando@isep.ipp.pt"));
        rb.setTopicosPericia(tpb);
        Revisor rc = new Revisor(new Utilizador("Marco", "123456", "Marco", "marco@isep.ipp.pt"));
        rc.setTopicosPericia(tpc);
        Revisor rd = new Revisor(new Utilizador("Renato", "123456", "Renato", "renato@isep.ipp.pt"));
        rd.setTopicosPericia(tpd);
        Revisor re = new Revisor(new Utilizador("Justino", "123456", "João", "joao@isep.ipp.pt"));
        re.setTopicosPericia(tpe);

        revisores.add(rc);

        Evento e = new Evento();
        CP cp = new CP();
        cp.setListaRevisores(revisores);
        e.setCp(cp);

        MecanismoDistribuicao2 instance = new MecanismoDistribuicao2();
        List<Revisor> expResult = revisores;
        List<Revisor> result = instance.getRevisoresRestantes(artb, revisores, minimoTopicosAfinidade);
        assertEquals(expResult, result);
    }

    /**
     * Test of setRevisoresArtigo method, of class MecanismoDistribuicao2.
     */
    @Test
    public void testSetRevisoresArtigo() {
        System.out.println("setRevisoresArtigo");

        int minimoRevisoresArtigo = 2;
        int minimoRevisoresAfinidade = 1;

        List<Topico> tpa = new ArrayList();
        tpa.add(new Topico("", "DOIS22"));
        tpa.add(new Topico("", "CINCO5"));
        tpa.add(new Topico("", "SEIS66"));
        tpa.add(new Topico("", "OITO88"));

        List<Topico> tpb = new ArrayList();
        tpb.add(new Topico("", "UM1111"));
        tpb.add(new Topico("", "DOIS22"));
        tpb.add(new Topico("", "OITO88"));
        tpb.add(new Topico("", "NOVE99"));

        List<Topico> tpc = new ArrayList();
        tpc.add(new Topico("", "UM1111"));
        tpc.add(new Topico("", "DOIS22"));
        tpc.add(new Topico("", "TRES33"));
        tpc.add(new Topico("", "QUATRO"));

        List<Topico> tpd = new ArrayList();
        tpd.add(new Topico("", "UM1111"));
        tpd.add(new Topico("", "TRES33"));
        tpd.add(new Topico("", "CINCO5"));
        tpd.add(new Topico("", "SETE77"));
        tpd.add(new Topico("", "NOVE99"));

        List<Topico> tpe = new ArrayList();
        tpe.add(new Topico("", "DOIS22"));
        tpe.add(new Topico("", "TRES33"));
        tpe.add(new Topico("", "QUATRO"));
        tpe.add(new Topico("", "SEIS66"));
        tpe.add(new Topico("", "OITO88"));

        Revisor ra = new Revisor(new Utilizador("Carol", "123456", "Carol", "carol@isep.ipp.pt"));
        ra.setTopicosPericia(tpa);
        Revisor rb = new Revisor(new Utilizador("Nando", "123456", "Fernado", "fernando@isep.ipp.pt"));
        rb.setTopicosPericia(tpb);
        Revisor rc = new Revisor(new Utilizador("Marco", "123456", "Marco", "marco@isep.ipp.pt"));
        rc.setTopicosPericia(tpc);
        Revisor rd = new Revisor(new Utilizador("Renato", "123456", "Renato", "renato@isep.ipp.pt"));
        rd.setTopicosPericia(tpd);
        Revisor re = new Revisor(new Utilizador("Justino", "123456", "João", "joao@isep.ipp.pt"));
        re.setTopicosPericia(tpe);

        List<Revisor> listaRevisoresAfinidade = new ArrayList();
        listaRevisoresAfinidade.add(rc);
        listaRevisoresAfinidade.add(ra);
        listaRevisoresAfinidade.add(rb);
        listaRevisoresAfinidade.add(re);
        listaRevisoresAfinidade.add(rd);

        List<Revisor> listaRevisoresRestante = new ArrayList();

        List<Revisor> listaTodosRevisoresAfinidade = new ArrayList();
        listaTodosRevisoresAfinidade.add(rc);
        listaTodosRevisoresAfinidade.add(ra);
        listaTodosRevisoresAfinidade.add(rb);
        listaTodosRevisoresAfinidade.add(rd);
        listaTodosRevisoresAfinidade.add(re);

        MecanismoDistribuicao2 instance = new MecanismoDistribuicao2();
        List<Revisor> expResult = new ArrayList();
        expResult.add(rc);
        expResult.add(ra);
        List<Revisor> result = instance.setRevisoresArtigo(minimoRevisoresArtigo, minimoRevisoresAfinidade, listaRevisoresAfinidade, listaRevisoresRestante, listaTodosRevisoresAfinidade);
        assertEquals(expResult, result);
    }

    /**
     * Test of getName method, of class MecanismoDistribuicao2.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        MecanismoDistribuicao2 instance = new MecanismoDistribuicao2();
        String expResult = "MecanismoDistribuicao2";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

}
